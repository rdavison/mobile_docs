using System.Runtime.InteropServices;
using Cirrious.CrossCore;
using MobileDocs.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.Security;
 
//using Windows.Security.Cryptography;
//using Windows.Storage.Streams;
//using Windows.Security.Cryptography.Core;
using System.Runtime.InteropServices.WindowsRuntime;
using Java.Security;
using Java.Lang;

namespace MobileDocs.Droid.Services
{
    public class DroidEncryptionService : IEncryptionService
    {

        private const string ConstSalt = "WAkPwyaT,X#)n{DZAq&|Vc0&ynOj (f(t0C1#Nh-G5)X?%l.<i*Zf%j)K/uR.crI";
        private const string Password = "G?14?EPak}~pg)C~wd=-4$a(gFp1<fI>Laxve]MESW~!z|b|8Rfl;Yj7`y/k{fWl";
        private const uint IterationCount = 10000;

        private static void PadToMultipleOf(ref byte[] src, int pad)
        {
            int len = (src.Length + pad - 1) / pad * pad;
            Array.Resize(ref src, len);
        }

        
        public byte[] Encrypt(byte[] toEncrypt, string salt)
        {

            return toEncrypt;
        }

        public byte[] Decrypt(byte[] cipher, string salt)
        {

            return cipher;
        }



        public string Encrypt(string toEncrypt, string salt)
        {
            byte[] encryptedBytes = Encrypt(Encoding.UTF8.GetBytes(toEncrypt), salt);
            return Convert.ToBase64String(encryptedBytes);
            //return Encoding.UTF8.GetString(encryptedBytes, 0, encryptedBytes.Length);
        }

        public string Decrypt(string cipher, string salt)
        {
            byte[] decryptedBytes = Decrypt(Convert.FromBase64String(cipher), salt);
            //remove empty bytes caused by encryption padding
            var i = decryptedBytes.Length - 1;
            while (decryptedBytes[i] == 0)
            {
                --i;
            }
            return Encoding.UTF8.GetString(decryptedBytes, 0, i + 1);
        }

        public string Sha256(string toHash)
        {
            MessageDigest digest = null;            
            digest = MessageDigest.GetInstance("SHA-256");
            digest.Update(Encoding.UTF8.GetBytes(toHash));
            return BytesToHexString(digest.Digest());
        }


        private static string BytesToHexString(byte[] bytes)
        {
            // http://stackoverflow.com/questions/332079
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < bytes.Length; i++)
            {
                string hex = Integer.ToHexString(0xFF & bytes[i]);
                if (hex.Length == 1)
                {
                    sb.Append('0');
                }
                sb.Append(hex);
            }
            return sb.ToString();
        }

    }
}
