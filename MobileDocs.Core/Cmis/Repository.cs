namespace MobileDocs.Cmis
{
    public class Repository
    {
        public Repository(string name, string rootUrl)
        {
            // TODO: Complete member initialization
            this.Name = name;
            this.RootUrl = rootUrl;
        }
        public string Name { get; private set; }
        public string RootUrl { get; private set; }
    }
}