using System;
using System.Collections.Generic;
using MobileDocs.Cmis;

namespace MobileDocs.Core.Cmis
{
    public class RepositorySuccessArgs : EventArgs
    {
        public List<Repository> Repositories { get; set; }
    }
}