using System;
using System.Collections.Generic;

namespace MobileDocs.Core.Cmis
{
    public class CmisPage
    {
        public DateTime LastModified { get; private set; }
        public string Name { get; private set; }
        public List<CmisFile> Files { get; private set; }

        public CmisPage(string name, List<CmisFile> files)
        {
            this.Name = name;
            this.Files = files;
        }
    }
}