using System;

namespace MobileDocs.Core.Cmis
{
    public class CmisFile
    {
        public string Name { get; set; }
        public string ObjectId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastModified { get; set; }
        public string CreatedBy { get; set; }
        public string Path { get; set; }
        public ulong Size { get; set; }
        public bool IsDirectory { get; set; }
        public bool IsRepository { get; set; }
        public string Repository { get; set; }
        public string BaseUrl { get; set; }

        private CmisFile(string id, string name, string repository, string path, string baseUrl)
        {
            this.ObjectId = id;
            this.Name = name;
            this.Path = path;
            this.Repository = repository;
            this.BaseUrl = baseUrl;
        }

        /// <summary>
        /// Construtor for file
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="path"></param>
        /// <param name="baseUrl"></param>
        /// <param name="lastModified"></param>
        /// <param name="size"></param>
        public CmisFile(string id, string name, string repository, string path, string baseUrl, DateTime lastModified, ulong size)
            : this(id, name, repository, path, baseUrl)
        {
            this.Size = size;
            this.LastModified = lastModified;
        }

        /// <summary>
        /// Construtor for folder
        /// </summary>
        /// <param name="id"></param>v
        /// <param name="name"></param>
        /// <param name="path"></param>
        /// <param name="baseUrl"></param>
        /// <param name="lastModified"></param>
        /// <param name="isDirectory"></param>
        public CmisFile(string id, string name, string repository, string path, string baseUrl, DateTime lastModified, bool isDirectory)
            : this(id, name, repository, path, baseUrl)
        {
            this.IsDirectory = isDirectory;
            this.LastModified = lastModified;
        }

        /// <summary>
        /// Construtor for repo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="path"></param>
        /// <param name="baseUrl"></param>
        /// <param name="repository"></param>
        public CmisFile(string id, string name, string path, string baseUrl, bool repository):this(id, name, name, "", baseUrl)
        {
            this.IsDirectory = true;
            this.IsRepository = repository;
        }

        public CmisFile()
        {
            
        }

        public string GetContentUrl()
        {
            return BaseUrl + "/" + Repository + "/root" + Path + "/" + Name;
        }
    }
}