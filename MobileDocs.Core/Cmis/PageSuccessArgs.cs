using System;

namespace MobileDocs.Core.Cmis
{
    public class PageSuccessArgs : EventArgs
    {
        public CmisPage Page { get; set; }
    }
}