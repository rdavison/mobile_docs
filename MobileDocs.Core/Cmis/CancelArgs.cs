using System;

namespace MobileDocs.Core.Cmis
{
    public class CancelArgs : EventArgs
    {
        public bool Cancel { get; set; }
    }
}