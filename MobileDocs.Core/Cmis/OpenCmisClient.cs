﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Net;
using System.Net.Http;
using System.Diagnostics;
using System.IO;
using MobileDocs.Core.Common;
using ModernHttpClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MobileDocs.Core.Cmis;

namespace MobileDocs.Core.Cmis
{

    public class MurmurHash2Simple
    {

        public static UInt32 Hash(string str)
        {
            return Hash(Encoding.UTF8.GetBytes(str));
        }

        public static UInt32 Hash(Byte[] data)
        {
            return Hash(data, 0xc58f1a7b);
        }
        const UInt32 M = 0x5bd1e995;
        const Int32 R = 24;

        public static UInt32 Hash(Byte[] data, UInt32 seed)
        {
            Int32 length = data.Length;
            if (length == 0)
                return 0;
            UInt32 h = seed ^ (UInt32)length;
            Int32 currentIndex = 0;
            while (length >= 4)
            {
                UInt32 k = BitConverter.ToUInt32(data, currentIndex);
                k *= M;
                k ^= k >> R;
                k *= M;

                h *= M;
                h ^= k;
                currentIndex += 4;
                length -= 4;
            }
            switch (length)
            {
                case 3:
                    h ^= BitConverter.ToUInt16(data, currentIndex);
                    h ^= (UInt32)data[currentIndex + 2] << 16;
                    h *= M;
                    break;
                case 2:
                    h ^= BitConverter.ToUInt16(data, currentIndex);
                    h *= M;
                    break;
                case 1:
                    h ^= data[currentIndex];
                    h *= M;
                    break;
                default:
                    break;
            }

            // Do a few final mixes of the hash to ensure the last few
            // bytes are well-incorporated.

            h ^= h >> 13;
            h *= M;
            h ^= h >> 15;

            return h;
        }
    }

    public class OpenCmisClient
    {

        private static string RootKey = "[ROOT_KEY]";

        public delegate void CancelEventHandler(object sender, CancelArgs e);
        public delegate void ErrorEventHandler(object sender, ErrorArgs e);
        public delegate void PageLoadedEventHandler(object sender, PageSuccessArgs e);
        public delegate void RepositoryLoadedEventHandler(object sender, RepositorySuccessArgs e);

        public event RepositoryLoadedEventHandler RepositoryLoaded;
        public event PageLoadedEventHandler PageLoaded;
        public event ErrorEventHandler CmisError;
        public event CancelEventHandler CmisCancel;


        //TODO add repo name to keys in cache
        private Dictionary<string, CmisPage> cache = new Dictionary<string, CmisPage>();

        private readonly HttpClient _client;
        private readonly LinkedList<String> _pagePaths = new LinkedList<string>();
        private String _repositoryName;

        private readonly string _username;
        private readonly string _password;
        public string BrowseUrl { get; set; }

        private int _treeSize = 0;

        [JsonConstructor]
        public OpenCmisClient(string username, string password, string browseUrl)
        {
            _username = username;
            _password = password;
            BrowseUrl = browseUrl;
            _client = new HttpClient(new NativeMessageHandler() { AllowAutoRedirect = true });
            //_client.Timeout = new TimeSpan(5000);
        }

        public OpenCmisClient Navigate(CmisFile folder)
        {
            if (!(folder.IsDirectory || folder.IsRepository))
            {
                throw new NotSupportedException("Navigate must be called from a repository OR a folder");
            }
            if (folder.IsRepository)
            {
                _pagePaths.Clear();
                _repositoryName = folder.Repository;
                return this;
            }
            else
            {
                _repositoryName = folder.Repository;
                return Navigate(folder.Path);
            }
        }

        public OpenCmisClient Navigate(string path)
        {
            if (path == "" || path == "/") //root path
            {
                return Root();
            }
            if (path.StartsWith("/")) //remove leading slash
            {
                path = path.Substring(1);
            }
            if (path.EndsWith("/")) //remove trailing slash
            {
                path = path.Remove(path.Length - 1);
            }
            if (path.Contains("/"))
            {
                return Navigate(path.Split('/'));
            }
            else
            {
                _pagePaths.Clear();
                _treeSize++;
                _pagePaths.AddLast(path);
                return this;
            }
        }

        public OpenCmisClient Root()
        {
            _repositoryName = null;
            _pagePaths.Clear();
            return this;
        }

        public OpenCmisClient Navigate(params string[] values)
        {
            _pagePaths.Clear();
            _treeSize = 0;
            foreach (string path in values)
            {
                
                _treeSize++;
                _pagePaths.AddLast(path);
            }
            return this;
        }

        public OpenCmisClient Up()
        {
            _treeSize--;
            _pagePaths.RemoveLast();
            return this;
        }

        public OpenCmisClient Up(CmisFile cmisFile)
        {
            string[] paths = cmisFile.Path.Substring(1).Split('/');
            string[] paths2 = new string[paths.Length-1];
            paths.CopyTo(paths2, paths.Length - 1);

            return Navigate(paths2);
            //return Navigate(paths.Take<string>(paths.Length-1).ToArray());
        }

        private async Task<List<CmisFile>> FetchRepositoriesAsync()
        {
            string jsonString = await GetUrlContentAsync(BrowseUrl);
            //JsonObject jsonObj = JsonObject.Parse(jsonString);
            List<CmisFile> repos = new List<CmisFile>();
            int i = 0;
            JObject jsonObj = JObject.Parse(jsonString);
            foreach (var x in jsonObj)
            {
                JToken repo = x.Value;
                string repoRootUrl = repo.Value<string>("rootFolderUrl");
                string repoName = repo.Value<string>("repositoryName");
                string rootPath = repoRootUrl.Substring(BrowseUrl.Length);
                repos.Add(new CmisFile(MurmurHash2Simple.Hash(repoRootUrl).ToString(), repoName, rootPath, BrowseUrl, true));
                i++;
            }
            return repos;
        }

        public string GetCurrentName()
        {
            if (_treeSize == 0)
            {
                return "root";
            }
            return _pagePaths.Last.Value;
        }

        public async Task<List<CmisFile>> BrowseAsync()
        {

            if (_repositoryName == null)
            {
                return await FetchRepositoriesAsync();
            }

            string relUrl = "";
            foreach (string path in _pagePaths)
            {
                relUrl +=  "/"+ path;
            }

            var pageUrl = BrowseUrl + "/" + _repositoryName + "/root" + relUrl;

           

            string jsonString = await GetUrlContentAsync(pageUrl);

            JObject jsonObj = JObject.Parse(jsonString);
            //JsonObject jsonObj = JsonObject.Parse(jsonString);
            List<CmisFile> files = new List<CmisFile>();

            foreach (var arrayEntry in jsonObj["objects"])
            {
                JToken props = arrayEntry["object"]["properties"];
                string id;
                if (props["cmis:secondaryObjectTypeIds"]["value"].HasValues)
                {
                    id = props["cmis:secondaryObjectTypeIds"]["value"].First().Value<string>();
                }else{
                    id = MurmurHash2Simple.Hash(props["cmis:objectId"].Value<string>("value")).ToString();
                }
                string name = props["cmis:name"].Value<string>("value");
                string type = props["cmis:baseTypeId"].Value<string>("value");
                DateTime lastModifed = new DateTime(props["cmis:lastModificationDate"].Value<long>("value") * 10000);
               
                if (type.Equals("cmis:folder"))
                {
                    string path = props["cmis:path"].Value<string>("value");

                    files.Add(new CmisFile(id, name, _repositoryName, path, BrowseUrl, lastModifed, true));
                }
                else
                {
                    int size = props["cmis:contentStreamLength"].Value<int?>("value") ?? 0;
                    files.Add(new CmisFile(id, name, _repositoryName,relUrl, BrowseUrl, lastModifed, Convert.ToUInt64(size)));
                }
            }
            return files;
        }

        public async Task<Stream> GetStream(string url)
        {
            return await GetStream(url, CancellationToken.None, null);
        }

        public async Task<Stream> GetStream(string url, CancellationToken cancelToken, Action<int> progressFunc = null, long size = 0)
        {
            Debug.WriteLine("====PAGEURL:====");
            Debug.WriteLine(url);
            Debug.WriteLine("=====");
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add("Authorization", "Basic " + System.Convert.ToBase64String(Encoding.UTF8.GetBytes(_username + ":" + _password)));
            //MeasureUtil.StartMeasure();
            
           // HttpRe
            HttpResponseMessage responseMessage =
                await _client.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cancelToken);//SendAsync(req, (CancellationToken) cancelToken);
            responseMessage.EnsureSuccessStatusCode();
            //MeasureUtil.StopMeasure("Send async");

            Debug.WriteLine(responseMessage.Content.GetType());

            //MeasureUtil.StartMeasure();
            Stream responseStream = await responseMessage.Content.ReadAsStreamAsync();
            //MeasureUtil.StopMeasure("ReadAsStreamAsync");
            if (progressFunc == null)
            {
                return responseStream;
            }
            MemoryStream stream = new MemoryStream();
            await Task.Run(async () =>
            {
                int read = 0;
                int progress = 0;
                int lastProgress = -1;
                double dOffset = 0;
                int offset = 0;

                byte[] responseBuffer = new byte[512];
                //long? contentLength = responseMessage.Content.Headers.ContentLength;
                double totalBytes = size;//contentLength ?? ;

                do
                {
                    if (cancelToken.IsCancellationRequested)
                    {
                        cancelToken.ThrowIfCancellationRequested();
                    }
                    //MeasureUtil.StartMeasure();
                    read = await responseStream.ReadAsync(responseBuffer, 0, responseBuffer.Length, cancelToken);
                    //MeasureUtil.StopMeasure("Read");
                    //MeasureUtil.StartMeasure();
                    await stream.WriteAsync(responseBuffer, 0, read, cancelToken);
                    //MeasureUtil.StopMeasure("Write");

                    offset += read;
                    dOffset += offset;
                    progress = (int) (offset/totalBytes*100);                
                    if (progress > lastProgress)
                    {
                        Debug.WriteLine("PROGRESS UPDATE == "+progress);
                        progressFunc(progress);
                    }
                    lastProgress = progress;          
                } while (read != 0);
            });
            return stream;
        }

        public async Task<string> GetUrlContentAsync(string url)
        {
            StreamReader reader = new StreamReader(await GetStream(url));
            return await reader.ReadToEndAsync();
        }
    }



    //public class JsonHelper
    //{
    //    public static JsonObject getJsonObject(JsonValue value, params string[] path)
    //    {
    //        return interate(value, path).GetObject();
    //    }

    //    public static JsonValue getJsonValue(JsonValue value, params string[] path)
    //    {
    //        return interate(value, path);
    //    }

    //    public static string getJsonString(JsonValue value, params string[] path)
    //    {
    //        return interate(value, path).GetString();
    //    }

    //    public static bool getJsonBoolean(JsonValue value, params string[] path)
    //    {
    //        return interate(value, path).GetBoolean();
    //    }

    //    public static double getJsonNumber(JsonValue value, params string[] path)
    //    {
    //        return interate(value, path).GetNumber();
    //    }

    //    public static JsonArray getJsonArray(JsonValue value, params string[] path)
    //    {
    //        return interate(value, path).GetArray();
    //    }

    //    private static JsonValue interate(JsonValue current, string[] path)
    //    {
    //        JsonObject obj;
    //        foreach (string name in path)
    //        {
    //            obj = current.GetObject();
    //            current = (JsonValue)obj[name];
    //        }
    //        return current;
    //    }
    //}
}
