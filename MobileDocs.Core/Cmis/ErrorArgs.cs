using System;

namespace MobileDocs.Core.Cmis
{
    public class ErrorArgs : EventArgs
    {
        public Exception Error { get; set; }
    }
}