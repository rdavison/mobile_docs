using Cirrious.CrossCore.Plugins;

namespace MobileDocs.Core.Bootstrap
{
    public class SqlitePluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Community.Plugins.Sqlite.PluginLoader>
    {
    }
}