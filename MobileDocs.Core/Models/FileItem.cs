﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MobileDocs.Core.Models
{
    public abstract class FileItem
    {
        public abstract string Name { get; }
        public abstract string Path { get; }
        public abstract string Id { get; }
        public abstract ulong Size { get; set; }
        public abstract object Object { get; set; }
        public abstract DateTime LastModified { get; set; }
        public abstract bool IsDirectory { get; }
        public abstract bool IsSynchronized { get; set; }
        public abstract bool IsFavorite { get; set; }

        public int ChildCount { get; set; }


        public string LastModifiedText
        {
            get
            {
                if (LastModified == DateTime.MinValue)
                {
                    return null;
                }
                return LastModified.ToString(System.Globalization.DateTimeFormatInfo.CurrentInfo.LongDatePattern);
            }
        }

        public string SizeText
        {
            get
            {
                return FormatSize(Size);
            }
        }

        private string FormatSize(UInt64 len)
        {
            string[] sizes = { "B", "KB", "MB", "GB" };
            int order = 0;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }
            return String.Format("{0:0.##} {1}", len, sizes[order]);
        }

        public string NameTurncated
        {
            get
            {
                string name = Name;
                if (name.Length > 60)
                {
                    string name1 = name.Substring(0, 44);
                    string name2 = name.Substring(name.Length - 13, 13);
                    return name1 + "..." + name2;
                }
                else
                {
                    return name;
                }
            }
        }

        public string Icon
        {
            get
            {
                string ext = System.IO.Path.GetExtension(Name);
                if (!string.IsNullOrEmpty(ext))
                {
                    return "icon_" + ext.Substring(1) + ".png";
                }
                return null;
            }
        }
    }
}
