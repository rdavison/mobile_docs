﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MobileDocs.Core.Models
{
    public enum ModuleType
    {
        Cmis, Link, Offline, Bookmark, Logout,
        Favorites
    }

    public class ProfileModule
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "moduleType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ModuleType Type { get; set; }

        [JsonProperty(PropertyName = "urlBrowser")]
        public string UrlBrowser { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "repository")]
        public string Repository { get; set; }

        [JsonProperty(PropertyName = "repoName")]
        public string RepoName { get; set; }

        private bool _enabled = true;
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }
        

        public string Icon
        {
            get
            {
                switch (Type)
                {
                    case ModuleType.Logout:
                        return "logout.png";
                    case ModuleType.Offline:
                        return "offline_storage.png";
                        break;
                    case ModuleType.Link:
                        return "external_link.png";
                        break;
                    case ModuleType.Favorites:
                        return "star.png";
                    case ModuleType.Cmis:
                        return "cloud_storage.png";
                    default:
                        return "";
                }
            }
        }
    }
}
