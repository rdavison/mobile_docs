﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Models
{
    public enum SortBy
    {
        Name,
        Date,
        Size
    }

    public class SortOption
    {
        public SortOption(string name, SortBy sortBy)
        {
            Name = name;
            SortBy = sortBy;
        }

        public string Name { get; private set; }
        public SortBy SortBy { get; private set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
