﻿using MobileDocs.Core.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MobileDocs.Core.Models
{
    public class Profile
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "logo")]
        public string Logo { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "color")]
        public string AccentColor { get; set; }

        [JsonProperty(PropertyName = "rssFeed")]
        public string RssFeed { get; set; }

        [JsonProperty(PropertyName = "background")]
        public string BackgroundImage { get; set; }

         [JsonProperty(PropertyName = "lastModified")]
        public long LastModified { get; set; }

        [JsonProperty(PropertyName = "modules")]
        public List<ProfileModule> Modules { get; set; }

        [JsonIgnore]
        public List<FeedItem> News { get; set; }
    }
}
