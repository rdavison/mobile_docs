﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileDocs.Core.Cmis;
using Newtonsoft.Json;

namespace MobileDocs.Core.Models
{
    class RemoteFileItem : FileItem
    {
        public RemoteFileItem()
        {

        }

        public RemoteFileItem(CmisFile file)
        {
            this._file = file;
        }

        private CmisFile _file;

        public override string Id
        {
            get { return _file.ObjectId; }
        }

        public override string Name
        {
            get { return _file.Name; }
        }

        public override string Path
        {
            get { return _file.Repository + _file.Path; }
        }

        public override ulong Size
        {
            get { return _file.Size; }
            set { }
        }

        public override object Object
        {
            get { return _file; }
            set { this._file = value as CmisFile; }
        }

        public override DateTime LastModified
        {
            get { return _file.LastModified; }
            set { }
        }

        public override bool IsDirectory
        {
            get { return _file.IsDirectory; }
        }
        
        public override bool IsSynchronized { get; set; }
        public override bool IsFavorite { get; set; }
    }
}
