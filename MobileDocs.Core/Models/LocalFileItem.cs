﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileDocs.Core.Database;

namespace MobileDocs.Core.Models
{
    public class LocalFileItem : FileItem
    {
        private Item _item;

        public LocalFileItem()
        {
            
        }

        public LocalFileItem(Item item)
        {
            _item = item;
        }

        public override string Name
        {
            get { return _item.Name; }
        }

        public override string Path
        {
            get { return null; }
        }

        public override string Id
        {
            get { return _item.ExternalId; }
        }

        public override ulong Size
        {
            get { return (ulong)_item.Size; }
            set { }
        }

        public override object Object
        {
            get { return _item; }
            set
            {
                    _item = value as Item;
            }
        }

        public override DateTime LastModified
        {
            get { return _item.LastModified; }
            set { }
        }

        public override bool IsDirectory
        {
            get { return _item.IsDirectory; }
        }

        public override bool IsSynchronized
        {
            get { return _item.IsSynchronized; }
            set { _item.IsSynchronized = value; }
        }

        public override bool IsFavorite
        {
            get { return _item.IsFavorite; }
            set { _item.IsFavorite = value; }
        }


    }
}
