﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Common
{
    public class Theme
    {

        static Theme()
        {
            AccentColor = "#e31937";
        }

        private static string _accentColor;
        private static string _accentColorLight;

        public static string AccentColorLight
        {
            get { return _accentColorLight; }
        }

        public static string AccentColor
        {
            get
            {
                return _accentColor;
            }
            set
            {
                _accentColorLight = "#99" + value.TrimStart('#');
                _accentColor = value;
            }
        }
    }
}
