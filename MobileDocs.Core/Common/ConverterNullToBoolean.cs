﻿using System.Collections;
using Cirrious.CrossCore.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Common
{
    public class ConverterNullToBoolean : IMvxValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool invert = false;
            if (parameter != null)
            {
                bool.TryParse(parameter.ToString(), out invert);
            }        
            if (value != null && value is ICollection)
            {
                return ((ICollection) value).Count > 0 || invert;
            }
            return value != null || invert;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
