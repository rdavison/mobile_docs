﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using MobileDocs.Core.Cmis;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;
using Newtonsoft.Json;

namespace MobileDocs.Core.Common
{
    public class CmisFileManager : IFileManager
    {
        private const string Url = "url";
        public OpenCmisClient Client { get; set; }

        //needed for reflection
        public CmisFileManager()
        {
            
        }

        public CmisFileManager(OpenCmisClient client)
        {
            Client = client;
        }

        public async Task<List<FileItem>> GetChildrenAsync(FileItem file)
        {

            if (!file.IsDirectory)
            {
                throw new NotSupportedException("Only directorys can list their children");
            }
            List<CmisFile> cmisFiles = await Client.Navigate(file.Object as CmisFile).BrowseAsync();
            List<FileItem> list = FromFiles(cmisFiles);
            list.Sort((x, y) => y.IsDirectory.CompareTo(x.IsDirectory));
            return list;
        }

        public async Task<List<FileItem>> GetRoot()
        {
            List<CmisFile> filesList = await Client.Root().BrowseAsync();
            return FromFiles(filesList);
        } 

        private List<FileItem> FromFiles(List<CmisFile> fileList)
        {
            List<FileItem> files = new List<FileItem>(fileList.Count);
            foreach (CmisFile currCmisFile in fileList)
            {
                files.Add(new RemoteFileItem(currCmisFile));
            }
            return files;
        }

        public async Task<Stream> GetContentStream(FileItem item, CancellationToken cancellationToken, Action<int> progressFunc)
        {
            CmisFile file = item.Object as CmisFile;
            return await Client.GetStream(file.GetContentUrl(), cancellationToken, progressFunc, (long)file.Size);
        }

        public async Task<string> GetFileContentsAsync(string path)
        {
            return await Client.GetUrlContentAsync(path);
        }


        public Dictionary<String, String> GenerateBundleArgs()
        {
            Dictionary<string, string> bundleArgs = new Dictionary<string, string>();
            bundleArgs[Url] = Client.BrowseUrl;
            return bundleArgs;
        }

        public IFileManager From(Dictionary<string, string> bundle)
        {
            ILoginService loginService = Mvx.Resolve<ILoginService>();
            CmisFileManager cmisFileManager = new CmisFileManager(new OpenCmisClient(loginService.Username,loginService.Password,bundle[Url]));
            return cmisFileManager;
        }
    }
}
