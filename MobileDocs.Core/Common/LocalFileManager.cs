﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cirrious.CrossCore;
using MobileDocs.Core.Database;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;

namespace MobileDocs.Core.Common
{
    public class LocalFileManager : IFileManager
    {

        //needed for reflection
        public LocalFileManager()
        {
            
        }

        private readonly IDatabaseService _databaseService;
        public LocalFileManager(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public async Task<List<FileItem>> GetChildrenFromId(int id)
        {
            List<FileItem> items = new List<FileItem>();
            await Task.Run(() =>
            {
                List<Item> files = _databaseService.Items(id);
                items.AddRange(files.Select(file => new LocalFileItem(file)));
                items.Sort((x, y) => y.IsDirectory.CompareTo(x.IsDirectory));
            });          
            return items;
        }

        public Task<List<FileItem>> GetChildrenAsync(FileItem item)
        {
            Item itemObject = item.Object as Item;
            return GetChildrenFromId(itemObject.Id);
        }

        public Task<List<FileItem>> GetRoot()
        {
            return GetChildrenFromId(_databaseService.RootItem.Id);
        }

        public async Task<Stream> GetContentStream(FileItem item, CancellationToken cancellationToken, Action<int> progressFunc)
        {
            return null;
        }

        public Task<string> GetFileContentsAsync(string path)
        {
            return null;
        }


        public Dictionary<String, String> GenerateBundleArgs()
        {
           return null;
        }

        public IFileManager From(Dictionary<string, string> bundleArgs)
        {
            IDatabaseService dbService = Mvx.Resolve<IDatabaseService>();
            return new LocalFileManager(dbService);
        }
    }
}
