﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Common
{

    public class OrderedHashSet<T> : KeyedCollection<T, T>
    {
        protected override T GetKeyForItem(T item)
        {
            return item;
        }
    }

    public class SparseBooleanArray
    {

        private int[] mKeys;
        private bool[] mValues;
        private int mSize;

        public SparseBooleanArray(int initialCapacity)
        {
            initialCapacity = ArrayUtils.IdealIntArraySize(initialCapacity);
            mKeys = new int[initialCapacity];
            mValues = new bool[initialCapacity];
            mSize = 0;
        }

        public SparseBooleanArray() : this(10)
        {

        }

        public bool Get(int key)
        {
            return Get(key, false);
        }

        public bool Get(int key, bool valueIfKeyNotFound)
        {
            int i = BinarySearch(mKeys, 0, mSize, key);

            if (i < 0)
            {
                return valueIfKeyNotFound;
            }
            else
            {
                return mValues[i];
            }
        }

        public void Delete(int key)
        {
            int i = BinarySearch(mKeys, 0, mSize, key);

            if (i >= 0)
            {
                Array.Copy(mKeys, i + 1, mKeys, i, mSize - (i + 1));
                Array.Copy(mValues, i + 1, mValues, i, mSize - (i + 1));
                mSize--;
            }
        }

        public void Put(int key, bool value)
        {
            int i = BinarySearch(mKeys, 0, mSize, key);

            if (i >= 0)
            {
                mValues[i] = value;
            }
            else
            {
                i = ~i;

                if (mSize >= mKeys.Length)
                {
                    int n = ArrayUtils.IdealIntArraySize(mSize + 1);

                    int[] nkeys = new int[n];
                    bool[] nvalues = new bool[n];

                    // Log.e("SparseBooleanArray", "grow " + mKeys.length + " to " + n);
                    Array.Copy(mKeys, 0, nkeys, 0, mKeys.Length);
                    Array.Copy(mValues, 0, nvalues, 0, mValues.Length);

                    mKeys = nkeys;
                    mValues = nvalues;
                }

                if (mSize - i != 0)
                {
                    // Log.e("SparseBooleanArray", "move " + (mSize - i));
                    Array.Copy(mKeys, i, mKeys, i + 1, mSize - i);
                    Array.Copy(mValues, i, mValues, i + 1, mSize - i);
                }

                mKeys[i] = key;
                mValues[i] = value;
                mSize++;
            }
        }

        public int Count()
        {
            return mSize;
        }

        public int KeyAt(int index)
        {
            return mKeys[index];
        }

        public bool ValueAt(int index)
        {
            return mValues[index];
        }

        public int IndexOfKey(int key)
        {
            return BinarySearch(mKeys, 0, mSize, key);
        }

        public int IndexOfValue(bool value)
        {
            for (int i = 0; i < mSize; i++)
                if (mValues[i] == value)
                    return i;

            return -1;
        }

        public void Clear()
        {
            mSize = 0;
        }

        public void Append(int key, bool value)
        {
            if (mSize != 0 && key <= mKeys[mSize - 1])
            {
                Put(key, value);
                return;
            }

            int pos = mSize;
            if (pos >= mKeys.Length)
            {
                int n = ArrayUtils.IdealIntArraySize(pos + 1);

                int[] nkeys = new int[n];
                bool[] nvalues = new bool[n];

                // Log.e("SparseBooleanArray", "grow " + mKeys.length + " to " + n);
                Array.Copy(mKeys, 0, nkeys, 0, mKeys.Length);
                Array.Copy(mValues, 0, nvalues, 0, mValues.Length);

                mKeys = nkeys;
                mValues = nvalues;
            }

            mKeys[pos] = key;
            mValues[pos] = value;
            mSize = pos + 1;
        }

        private static int BinarySearch(int[] a, int start, int len, int key)
        {
            int high = start + len, low = start - 1, guess;

            while (high - low > 1)
            {
                guess = (high + low) / 2;

                if (a[guess] < key)
                    low = guess;
                else
                    high = guess;
            }

            if (high == start + len)
                return ~(start + len);
            else if (a[high] == key)
                return high;
            else
                return ~high;
        }

        private void CheckIntegrity()
        {
            for (int i = 1; i < mSize; i++)
            {
                if (mKeys[i] <= mKeys[i - 1])
                {
                    for (int j = 0; j < mSize; j++)
                    {
                        //Log.e("FAIL", j + ": " + mKeys[j] + " -> " + mValues[j]);
                    }

                    throw new Exception();
                }
            }
        }


        
    }

    public class ArrayUtils
    {
        private static object[] EMPTY = new object[0];
        private static readonly int CACHE_SIZE = 73;
        private static object[] sCache = new object[CACHE_SIZE];

        private ArrayUtils()
        {
            /* cannot be instantiated */
        }

        public static int IdealByteArraySize(int need)
        {
            for (int i = 4; i < 32; i++)
                if (need <= (1 << i) - 12)
                    return (1 << i) - 12;

            return need;
        }

        public static int IdealIntArraySize(int need)
        {
            return IdealByteArraySize(need * 4) / 4;
        }
    }
}
