﻿using Cirrious.CrossCore.Core;
using MobileDocs.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Common
{
    public class ErrorApplicationObject : MvxMainThreadDispatchingObject, IErrorReporter, IErrorSource
    {
        public void ReportError(string error)
        {
            if (ErrorReported == null){
                return;
            }
            InvokeOnMainThread(() =>
            {
                var handler = ErrorReported;
                if (handler != null) { 
                    handler(this, new ErrorEventArgs(error));
                }
            });

        }

        public event EventHandler<ErrorEventArgs> ErrorReported;
    }
}
