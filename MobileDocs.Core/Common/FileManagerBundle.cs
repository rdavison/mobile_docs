﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileDocs.Core.Interfaces;
using Newtonsoft.Json;

namespace MobileDocs.Core.Common
{
    public class FileManagerBundle
    {
        public FileManagerBundle(Type managerType, Dictionary<string, string> args)
        {
            ManagerType = managerType;
            Args = args;
        }

        public Dictionary<String, String> Args { get; private set; }
        public Type ManagerType { get; private set; }

        public static IFileManager From(FileManagerBundle bundle)
        {
            IFileManager fm = (IFileManager)Activator.CreateInstance(bundle.ManagerType);
            fm = fm.From(bundle.Args);
            return fm;
        }

        public static FileManagerBundle Generate(IFileManager manager)
        {
            return new FileManagerBundle(manager.GetType(),manager.GenerateBundleArgs());
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static IFileManager FromJson(string json)
        {
            if (json == null)
            {
                return null;
            }
            return FileManagerBundle.From(JsonConvert.DeserializeObject<FileManagerBundle>(json));
        }


    }
}
