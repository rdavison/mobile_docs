﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cirrious.CrossCore;
using HtmlAgilityPack;
using MobileDocs.Core.Net;

namespace MobileDocs.Core.Common
{

    /// <summary>
    /// Represents a feed item.
    /// </summary>
    public class FeedItem
    {
        public string Link { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Excerpt { get; set; }
        public DateTime PublishDate { get; set; }
        public FeedType FeedType { get; set; }
        public string ImageUrl { get; set; }

        public FeedItem()
        {
            Link = "";
            Title = "";
            Content = "";
            PublishDate = DateTime.Today;
            FeedType = FeedType.RSS;
        }

        public string Meta
        {
            get
            {
                return this.PublishDate.ToString(System.Globalization.DateTimeFormatInfo.CurrentInfo.LongDatePattern);
            }
        }
    }

    /// <summary>
    /// A simple RSS, RDF and ATOM feed parser.
    /// </summary>
    public class FeedParser
    {

        /// <summary>
        /// Parses the given <see cref="FeedType"/> and returns a <see cref="IList&amp;lt;Item&amp;gt;"/>.
        /// </summary>
        /// <returns></returns>
        public List<FeedItem> Parse(string url)
        {
            try {

                XDocument doc = XDocument.Load(url);
                FeedType feedType = FeedType.Unknown;
                if (doc.Root.Name.LocalName == "rss")
                {
                    feedType = FeedType.RSS;
                }
                else if (doc.Root.Name.LocalName == "feed")
                {
                    feedType = FeedType.Atom;
                }
                else if(doc.Root.Name.LocalName == "rdf")
                {
                    feedType = FeedType.RDF;
                }
                switch (feedType)
                {
                    case FeedType.RSS:
                        return Clean(ParseRss(doc));
                    case FeedType.RDF:
                        return Clean(ParseRdf(doc));
                    case FeedType.Atom:
                        return Clean(ParseAtom(doc));
                    default:
                        throw new NotSupportedException(string.Format("{0} is not supported", feedType.ToString()));
                }
            }
            catch (Exception ex)
            {
                return new List<FeedItem>();
            }

        }

        private List<FeedItem> Clean(List<FeedItem> list)
        {

            foreach (var item in list)
            {
                string imageUrl =
                                Regex.Match(item.Content, "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase)
                                    .Groups[1].Value;
                item.ImageUrl = string.IsNullOrEmpty(imageUrl) ? null : imageUrl;
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(item.Content);
                string excerpt = doc.DocumentNode.InnerText.Replace("\n", "").TrimStart();
                item.Excerpt = excerpt;
            }
            return list;
        }

        /// <summary>
        /// Parses an Atom feed and returns a <see cref="IList&amp;lt;Item&amp;gt;"/>.
        /// </summary>
        private List<FeedItem> ParseAtom(XDocument doc)
        {
                // Feed/Entry
                var entries = from item in doc.Root.Elements().Where(i => i.Name.LocalName == "entry")
                              select new FeedItem
                              {
                                  FeedType = FeedType.Atom,
                                  Content = item.Elements().First(i => i.Name.LocalName == "content").Value,
                                  Link = item.Elements().First(i => i.Name.LocalName == "link").Attribute("href").Value,
                                  PublishDate = ParseDate(item.Elements().First(i => i.Name.LocalName == "published").Value),
                                  Title = item.Elements().First(i => i.Name.LocalName == "title").Value
                              };
                return entries.ToList();
        }

        /// <summary>
        /// Parses an RSS feed and returns a <see cref="IList&amp;lt;Item&amp;gt;"/>.
        /// </summary>
        private List<FeedItem> ParseRss(XDocument doc)
        {

            XElement[] xElements =
                doc.Root.Descendants()
                    .First(i => i.Name.LocalName == "channel")
                    .Elements()
                    .Where(i => i.Name.LocalName == "item")
                    .ToArray();
            List<FeedItem> feedItems = new List<FeedItem>(xElements.Length);
            foreach (XElement item in xElements)
            {
                XElement[] itemElements = item.Elements().ToArray();
                FeedItem feedItem = new FeedItem()
                {
                    FeedType = FeedType.RSS,
                    Content = itemElements.First(i => i.Name.LocalName == "description").Value,
                    Link = itemElements.First(i => i.Name.LocalName == "link").Value,
                    Title = itemElements.First(i => i.Name.LocalName == "title").Value
                };
                XElement pubDate = itemElements.FirstOrDefault(i => i.Name.LocalName == "pubDate") ??
                                   itemElements.FirstOrDefault(i => i.Name.LocalName == "date");
                if (pubDate != null)
                {
                    feedItem.PublishDate = ParseDate(pubDate.Value);
                }
                feedItems.Add(feedItem);
            }
            return feedItems;
        }

        /// <summary>
        /// Parses an RDF feed and returns a <see cref="IList&amp;lt;Item&amp;gt;"/>.
        /// </summary>
        private List<FeedItem> ParseRdf(XDocument doc)
        {
                var entries = from item in doc.Root.Descendants().Where(i => i.Name.LocalName == "item")
                              select new FeedItem
                              {
                                  FeedType = FeedType.RDF,
                                  Content = item.Elements().First(i => i.Name.LocalName == "description").Value,
                                  Link = item.Elements().First(i => i.Name.LocalName == "link").Value,
                                  PublishDate = ParseDate(item.Elements().First(i => i.Name.LocalName == "date").Value),
                                  Title = item.Elements().First(i => i.Name.LocalName == "title").Value
                              };
                return entries.ToList();
        }

        private DateTime ParseDate(string date)
        {
            DateTime result;
            if (DateTime.TryParse(date, out result))
                return result;
            else
                return DateTime.MinValue;
        }
    }
    /// <summary>
    /// Represents the XML format of a feed.
    /// </summary>
    public enum FeedType
    {
        /// <summary>
        /// Really Simple Syndication format.
        /// </summary>
        RSS,
        /// <summary>
        /// RDF site summary format.
        /// </summary>
        RDF,
        /// <summary>
        /// Atom Syndication format.
        /// </summary>
        Atom,
        Unknown
    }
}
