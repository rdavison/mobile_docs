﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Common
{
    public interface IImageResizer
    {
        Task<byte[]> Resize(Stream stream, uint width, uint height, string extension, string encoding = null);
    }
}
