﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Common
{
    public static class MeasureUtil
    {
        private static long tick1 = 0;
        private static long tick2 = 0;

        public static void StartMeasure()
        {
            tick1 = DateTime.Now.Ticks;
        }

        public static void StopMeasure(string tag)
        {
            tick2 = DateTime.Now.Ticks;
            Debug.WriteLine("======");
            Debug.WriteLine(tag + " - TIME TO EXECUTE:"+new DateTime(tick2).Subtract(new DateTime(tick1)).TotalMilliseconds);
            Debug.WriteLine("======");
            tick1 = 0;
            tick2 = 0;
        }
    }
}
