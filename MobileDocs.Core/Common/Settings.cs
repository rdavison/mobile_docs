﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;
using MobileDocs.Core.Interfaces;
using Newtonsoft.Json;
using Refractored.MvxPlugins.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Common
{

    public class MurmurHash2Unsafe
    {
        public static UInt32 Hash(Byte[] dataToHash)
        {
            Int32 dataLength = dataToHash.Length;
            if (dataLength == 0)
                return 0;
            UInt32 hash = (UInt32)dataLength;
            Int32 remainingBytes = dataLength & 3; // mod 4
            Int32 numberOfLoops = dataLength >> 2; // div 4
            Int32 currentIndex = 0;
            while (numberOfLoops > 0)
            {
                hash += (UInt16)(dataToHash[currentIndex++] | dataToHash[currentIndex++] << 8);
                UInt32 tmp = (UInt32)((UInt32)(dataToHash[currentIndex++] | dataToHash[currentIndex++] << 8) << 11) ^ hash;
                hash = (hash << 16) ^ tmp;
                hash += hash >> 11;
                numberOfLoops--;
            }

            switch (remainingBytes)
            {
                case 3:
                    hash += (UInt16)(dataToHash[currentIndex++] | dataToHash[currentIndex++] << 8);
                    hash ^= hash << 16;
                    hash ^= ((UInt32)dataToHash[currentIndex]) << 18;
                    hash += hash >> 11;
                    break;
                case 2:
                    hash += (UInt16)(dataToHash[currentIndex++] | dataToHash[currentIndex] << 8);
                    hash ^= hash << 11;
                    hash += hash >> 17;
                    break;
                case 1:
                    hash += dataToHash[currentIndex];
                    hash ^= hash << 10;
                    hash += hash >> 1;
                    break;
                default:
                    break;
            }

            /* Force "avalanching" of final 127 bits */
            hash ^= hash << 3;
            hash += hash >> 5;
            hash ^= hash << 4;
            hash += hash >> 17;
            hash ^= hash << 25;
            hash += hash >> 6;

            return hash;
        }
    }

    public static class Settings
    {

        private static JsonSerializerSettings _jsonSerializeSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Objects,
            TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
        };

        private static JsonSerializerSettings _jsonDeserializeSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Objects,
        };

        private static ISettings _settings;
        private static ISettings AppSettings
        {
            get
            {
                return _settings ?? (_settings = _settings = Mvx.Resolve<ISettings>());
            }
        }

        private static IEncryptionService _encryption;
        private static IEncryptionService Encryption{
            get
            {
                return _encryption ?? (_encryption = Mvx.Resolve<IEncryptionService>());
            }
        }


	
		private static IPlatformFileSystem _separator;
		private static IPlatformFileSystem Separator
		{
			get
			{
				return _separator ?? (_separator = _separator = Mvx.Resolve<IPlatformFileSystem>());
			}
		}

        private static IMvxFileStore _fileStore;
        private static IMvxFileStore FileStore
        {
            get
            {
                return _fileStore ?? (_fileStore = _fileStore = Mvx.Resolve<IMvxFileStore>());
            }
        }

        #region Setting Constants

        /// <summary>
        /// Key for your setting
        /// </summary>
        
        //private
        private const string SelectedCompanyIndexKey = "selectedCompanyIndex";
        

        //public
        public const string KeyPassword = "passwordKey";
        public const string KeyUsername = "usernameKey";
        public const string KeySelectedSortIndex = "selectedSortIndex";
        public const string KeyProfileLastModified = "profileLastModifiedKey";

        public const string PathProfileData = "profile.dat";
        public const string PathCompanyData = "companies.dat";
        public const string PathNewsData = "news.dat";
        public const string PathSyncState = "syncqueue.dat";

        #endregion

        public static int GetInt(string key, string hash = null){
            if (hash == null)
            {
                return AppSettings.GetValueOrDefault<int>(key);
            }
            string str = GetString(key, hash);
            if (str == null)
            {
                return -1;
            }
            return int.Parse(str);
        }

        public static bool GetBool(string key, string hash = null)
        {
            if (hash == null)
            {
                return AppSettings.GetValueOrDefault<bool>(key);
            }
            string str = GetString(key, hash);
            bool result = false;
            bool.TryParse(str, out result);
            return result;
        }

        public static long GetLong(string key, string hash = null)
        {
            if (hash == null)
            {
                return AppSettings.GetValueOrDefault<long>(key);
            }
            string str = GetString(key, hash);
            if (str == null)
            {
                return -1;
            }
            return long.Parse(str);
        }

        public static string GetString(string key, string hash = null)
        {
            if (hash == null)
            {
                return AppSettings.GetValueOrDefault<string>(key);
            }
            string cipherString = AppSettings.GetValueOrDefault<string>(hash + "_" + key);
            if(cipherString == null){
                return null;
            }
            return Encryption.Decrypt(cipherString, hash);
        }

        public static void Save(string key, string value, string hash = null)
        {
            SaveInternal(key, value, hash);
        }

        public static void Save(string key, bool value, string hash = null)
        {
            SaveInternal(key, value, hash);
        }

        public static void Save(string key, int value, string hash = null)
        {
            SaveInternal(key, value, hash);
        }

        public static void Save(string key, long value, string hash = null)
        {
            SaveInternal(key, value, hash);
        }

        private static void SaveInternal(string key, object value, string hash = null)
        {
            if (hash == null)
            {
                AppSettings.AddOrUpdateValue(key, value);
            }
            else
            {
                string valueEncrypted = Encryption.Encrypt(value.ToString(), hash);
                AppSettings.AddOrUpdateValue(hash + "_" + key, valueEncrypted);
            }
            AppSettings.Save();     
        }




        public static void Encrypt<T>(Action<T> prop, T value, string username, string password)
        {
            
            prop(value);
            
        }

        public static T Decrypt<T>(Func<T> prop, string username, string password)
        {
            T value = prop();

            return value;
        }

        private static string ConvertPath(string path, string hash = null)
        {
            if (hash != null)
            {
                path = hash + "/" + path;
            }
			return path.Replace("/", Separator.Slash);
        }


        private static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static void Clear(string path, string hash = null, bool hashSpecific = false)
        {
            if (hashSpecific)
            {
                path = ConvertPath(path, hash);
            }
            else
            {
				path = path.Replace("/", Separator.Slash);
            }
            FileStore.DeleteFile(path);
        }


        public static void SaveAsJson<T>(T data, string path, string hash = null, bool hashSpecific = false)
        {
            string json = JsonConvert.SerializeObject(data, _jsonSerializeSettings);
            if (hashSpecific) { 
                path = ConvertPath(path, hash);
            }
            else
            {
				path = path.Replace("/", Separator.Slash);
            }
			if (path.Contains(Separator.Slash))
            { 
				FileStore.EnsureFolderExists(path.Substring(0,path.LastIndexOf(Separator.Slash)));
            }
            if (hash != null)
            {
                FileStore.WriteFile(path, Encryption.Encrypt(json, hash));
            }
            else
            {
                FileStore.WriteFile(path, json);
            }
            
        }

        public static T LoadFromJson<T>(string path, string hash = null, bool hashSpecific = false)
        {
            if (hashSpecific)
            {
                path = ConvertPath(path, hash);
            }
            else
            {
				path = path.Replace("/", Separator.Slash);
            }
            string json = null;
            if (!FileStore.TryReadTextFile(path, out json))
            {
                return default(T);
            }
            else
            {
                if (hash != null)
                {
                    return JsonConvert.DeserializeObject<T>(Encryption.Decrypt(json, hash), _jsonDeserializeSettings);
                }
                return JsonConvert.DeserializeObject<T>(json, _jsonDeserializeSettings);
            }
        }

        public static int SelectedCompanyIndex
        {
            get { return AppSettings.GetValueOrDefault<int>(SelectedCompanyIndexKey, 0);}
            set { AppSettings.AddOrUpdateValue(SelectedCompanyIndexKey, value); AppSettings.Save(); }
        }




        //public static string Password
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(PasswordKey); }
        //    set { AppSettings.AddOrUpdateValue(PasswordKey, value); AppSettings.Save(); }
        //}

        //public static string Username
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(UsernameKey); }
        //    set { AppSettings.AddOrUpdateValue(UsernameKey, value); AppSettings.Save(); }
        //}



        
    }
}
