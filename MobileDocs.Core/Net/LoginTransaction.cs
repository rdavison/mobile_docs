﻿using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Net
{
    public class LoginTransaction : TransactionBase
    {
        private readonly string _id;



        public LoginTransaction(string id, string username, string password) : base("Authorization", "Basic " + System.Convert.ToBase64String(Encoding.UTF8.GetBytes(username + ":" + password)))
        {
            _id = id;
        }



        public override string GetMethod()
        {
            return string.Concat("rest/admin/profile/", _id);
        }

        public override Type GetType()
        {
            return typeof(Profile);
        }

    }
}
