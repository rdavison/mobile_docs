﻿using System.Xml.Linq;
using ModernHttpClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Net
{
    public class NetworkClient
    {
        //
        HttpClient httpClient = new HttpClient(new NativeMessageHandler());

        public async Task<Stream> GetStream(string url)
        {
            return await GetStream(new HttpRequestMessage(HttpMethod.Get, url));
        }

        public async Task<Stream> GetStream(HttpRequestMessage req)
        {
            HttpResponseMessage response = await httpClient.SendAsync(req);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStreamAsync();
        }

        public async Task<string> GetString(string url)
        {
            StreamReader reader = new StreamReader(await GetStream(url));

            return await reader.ReadToEndAsync();
        }

        public async Task<byte[]> GetByteArray(string url)
        {
            MemoryStream memoryStream = new MemoryStream();
            (await this.GetStream(url)).CopyTo(memoryStream);
            return memoryStream.ToArray();
        }

    }
}
