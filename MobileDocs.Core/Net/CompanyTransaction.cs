﻿using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace MobileDocs.Core.Net
{
    public class CompanyTransaction : TransactionBase
    {
        private readonly string _id;

        public CompanyTransaction(string id)
        {
            _id = id;
        }

        public override string GetBaseUrl()
        {
            return "*********";
        }



        public override string GetMethod()
        {
            return string.Concat("md-company/rest/admin/company/", _id);
        }

        public override Type GetType()
        {
            return typeof(Company);
        }
    }
}
