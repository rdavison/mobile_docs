﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json;
using System.IO;

namespace MobileDocs.Core.Net
{
    public abstract class TransactionBase
    {

		public enum RequestType{
			GET,POST,OPTIONS
		}


        

        public List<RequestParameter> RequestParameters { get; set; }
        public List<RequestParameter> HeaderParameters { get; set; }

        public TransactionBase()
        {

        }


        public virtual string GetBaseUrl()
        {
            return null;
        }



        private List<RequestParameter> ListFromParams(params string[] queryParams)
        {
            List<RequestParameter> list = new List<RequestParameter>();
            string key = null;
            string value = null;
            int i = 0;
            foreach (string query in queryParams)
            {
                if (i % 2 == 0)
                {
                    key = query;
                }
                if (i % 2 == 1)
                {
                    value = query;
                    list.Add(new RequestParameter(key, value));
                }
                i++;
            }
            return list;
        }

        public void SetRequestParameters(params string[] queryParams)
        {
            RequestParameters = ListFromParams(queryParams);
        }

        public TransactionBase(params string[] headerParams)
        {
            HeaderParameters = ListFromParams(headerParams);
        }

        NetworkClient networkClient = new NetworkClient();

        private static string _baseUrl;

        public static string BaseUrl
        {
            get { return _baseUrl; }
            set
            {
                _baseUrl = value.EndsWith("/")?value:value + "/";
            }
        }


        public abstract string GetMethod();
        public abstract Type GetType();

        public virtual HttpMethod GetHttpMethod()
        {
            return HttpMethod.Get;
        }

        public async Task<T> ExecuteAsync<T>()
        {
            string url = (GetBaseUrl()==null?BaseUrl:GetBaseUrl()) + GetMethod();
            HttpMethod method = GetHttpMethod();

            if (method.Equals(HttpMethod.Get))
            {
                List<RequestParameter> queryParams = RequestParameters;
                if (queryParams != null)
                {
                    url += "?";
                    foreach (RequestParameter param in queryParams)
                    {
                        url += param.Key + "=" + param.Value + "&";
                    }
                    url = url.Substring(0, url.Length - 2);
                }
            }

            HttpRequestMessage req = new HttpRequestMessage(method, url);
		
            List<RequestParameter> headers = HeaderParameters;
            if (headers != null)
            {
                foreach (RequestParameter param in headers)
                {
                    req.Headers.Add(param.Key, param.Value);
                }
            }
            Stream stream = await networkClient.GetStream(req);
            StreamReader reader = new StreamReader(stream);
            return (T)JsonConvert.DeserializeObject(await reader.ReadToEndAsync(), GetType());
        }

    }
}
