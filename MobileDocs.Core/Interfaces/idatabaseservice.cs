﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileDocs.Core.Common;
using MobileDocs.Core.Database;
using MobileDocs.Core.Models;

namespace MobileDocs.Core.Interfaces
{
    public interface IDatabaseService
    {
        Item RootItem { get; set; }
        List<Item> Items(int id);
        void Store(FileItem item, string assetId, int parentId);
        DateTime ItemLastModified(string externalId);
        FileItem ExternalToId(string externalId);
        void Init(string dbName);
        bool IsFavorite(string externalId);
        void AddFavorite(string externalId);
        void RemoveFavorite(string externalId);
        void AddRecent(FileItem item, int id);
        List<FileItem> RecentFiles();
        List<FileItem> Favorites();
        OrderedHashSet<int> CreateFolders(FileItem[] folderPath, FileItem currentItem);
        void UpdateSyncFlag(OrderedHashSet<int> folderIds, int[] childCountPerFolder);
        bool HasFavorites();
    }
}
