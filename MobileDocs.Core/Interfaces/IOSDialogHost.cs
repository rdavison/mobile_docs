using Cirrious.MvvmCross.ViewModels;


namespace MobileDocs.Core.Interfaces
{
	public interface IOSDialogHost<out T> where T : MvxViewModel
	{
		T GetViewModel();
	}
}