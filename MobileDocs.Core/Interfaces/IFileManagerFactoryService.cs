﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Interfaces
{
    public interface IFileManagerFactoryService
    {
        IFileManager CreateManager(Type type, Dictionary<String, String> bundle = null);
    }
}
