﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Interfaces
{
    public interface IErrorReporter
    {
        void ReportError(string error);
    }
}
