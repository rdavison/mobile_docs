﻿using System.Threading.Tasks;
using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MobileDocs.Core.Interfaces
{
    public interface ICompanyService
    {
        Task<Company> CreateNewCompany(string id);
    }
}
