﻿using System;

namespace MobileDocs.Core
{
	public interface IPlatformFileSystem
	{
		String Slash {
			get;
		}

		String DataPath {
			get;
		}
	}
}

