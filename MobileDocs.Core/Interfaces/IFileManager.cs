﻿using System.Threading;
using MobileDocs.Core.Common;
using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Interfaces
{
    public interface IFileManager
    {
        Task<List<FileItem>> GetChildrenAsync(FileItem item);
        Task<List<FileItem>> GetRoot();
        //TODO FIX SO METHOD IS MORE GENENERIC
        Task<Stream> GetContentStream(FileItem item, CancellationToken token, Action<int> progressAction);
        Task<string> GetFileContentsAsync(string path);
        Dictionary<String, String> GenerateBundleArgs();
        IFileManager From(Dictionary<String, String> bundleArgs);
    }
}
