﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Interfaces
{
    public interface IEncryptionService
    {
        string Encrypt(string toEncrypt, string salt);
        byte[] Encrypt(byte[] toEncrypt, string salt);
        string Decrypt(string cipher, string salt);
        byte[] Decrypt(byte[] cipher, string salt);
        string Sha256(string toHash);
    }
}
