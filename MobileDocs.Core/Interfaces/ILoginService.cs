﻿using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Interfaces
{
    public interface ILoginService
    {
        bool LoggedIn { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        string UserPasswordHash { get; }
        string UserHash { get; }
        string CompanyHash { get; }
        Profile Profile { get; set; }
        Task<Profile> Login(string url, string companyId, string username, string password);
        void StartCountdown();
        void ComputeHashes(string companyId, string username, string password);
        void Logout();
        void InitDb();
    }
}
