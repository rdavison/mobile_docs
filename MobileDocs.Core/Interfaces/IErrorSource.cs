﻿using MobileDocs.Core.Common;
using System;

namespace MobileDocs.Core.Interfaces
{
    public interface IErrorSource
    {
        event EventHandler<ErrorEventArgs> ErrorReported;
    }
}
