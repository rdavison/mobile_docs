using Cirrious.CrossCore;
using Cirrious.CrossCore.IoC;
using MobileDocs.Core.Common;
using MobileDocs.Core.Interfaces;

namespace MobileDocs.Core
{
    public class App : Cirrious.MvvmCross.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
				
            RegisterAppStart<ViewModels.LoginViewModel>();
            
                
            var errorApplicationObject = new ErrorApplicationObject();
            
            Mvx.RegisterSingleton<IErrorReporter>(  errorApplicationObject);
            Mvx.RegisterSingleton<IErrorSource>(errorApplicationObject);
        }
    }
}