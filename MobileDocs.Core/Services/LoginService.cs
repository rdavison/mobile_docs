﻿using System.Threading;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using MobileDocs.Core.Common;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;
using MobileDocs.Core.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Core.Services
{
    public class LoginMessage : MvxMessage
    {
        public LoginMessage(object sender) : base(sender)
        {
            IsLoggedIn = false;
        }

        public bool IsLoggedIn { get; private set; }
    }

    public class LoginService : ILoginService
    {
        private Timer timer;
        private readonly IEncryptionService _encryption;
        private readonly IMvxMessenger _messenger;
        private readonly IDatabaseService _databaseService;

        public LoginService(IEncryptionService encryption, IDatabaseService databaseService, IMvxMessenger messenger)
        {
            _databaseService = databaseService;
            _encryption = encryption;
            _messenger = messenger;
        }

        public Profile Profile { get; set; }
        public string UserHash { get; private set; }
        public string UserPasswordHash { get; private set; }
        public string CompanyHash { get; private set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool LoggedIn { get; set; }

        public async Task<Profile> Login(string url, string companyId, string username, string password)
        {
            TransactionBase.BaseUrl = url;
            LoginTransaction transation = new LoginTransaction(companyId, username, password);
            Profile = await transation.ExecuteAsync<Profile>();
            if (Profile == null)
            {
                throw new Exception("Server responed but response is empty. Contact administrator.");
            }
            ComputeHashes(companyId, username, password);
            LoggedIn = true;
            StartCountdown();
            InitDb();
            return Profile;
        }

        public void ComputeHashes(string companyId, string username, string password)
        {
            Username = username;
            Password = password;
            UserHash = _encryption.Sha256(companyId + username);
            UserPasswordHash = _encryption.Sha256(companyId + username + password);
            CompanyHash = _encryption.Sha256(companyId);
        }


        public void Logout()
        {
            //timer.Dispose();
            UserHash = null;
            Profile = null;
            Username = null;
            Password = null;
            LoggedIn = false;
            _messenger.Publish(new LoginMessage(this));


            Mvx.Trace("LOGOUT TIMEOUT");
        }


        public void StartCountdown()
        {
            //timer = new Timer(TimerEnded, null, 10000, -1);
        }

        private void TimerEnded(object state)
        {       
            Logout();
        }


        public void InitDb()
        {
            _databaseService.Init(UserHash);
        }
    }
}
