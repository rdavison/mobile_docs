﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.MvvmCross.Plugins.Messenger;
using MobileDocs.Core.Cmis;
using MobileDocs.Core.Common;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;

namespace MobileDocs.Core.Services
{

    public class SyncMessage : MvxMessage
    {
        public SyncMessage(object sender, bool isComplete, string currentFileName, string currentFileId, int? progress = null) : base(sender)
        {
            IsComplete = isComplete;
            CurrentFileName = currentFileName;
            CurrentFileId = currentFileId;
            Progress = progress;
        }

        public SyncMessage(object sender, Exception exception) : base(sender)
        {
            Exception = exception;
        }

        public Exception Exception { get; private set; }

        public int? Progress { get; private set; }
        public bool IsComplete { get; private set; }
        public string CurrentFileName { get; private set; }
        public string CurrentFileId { get; private set; }
    }

    public class SyncState
    {
        public Queue<SyncItem> Queue { get; set; }
        public FileManagerBundle FileManagerBundle { get; set; }
        public LinkedList<FileItem> Children { get; set; }
        public string Id { get; set; }
        public SyncItem CurrentItem { get; set; }
        public int ChildrenAdded { get; set; }
    }

    public class SyncItem
    {
        public FileItem[] PathItems { get; set; }
        public FileItem Item { get; set; }

        public SyncItem(FileItem[] pathItems,FileItem item)
        {
            Item = item;
            PathItems = pathItems;
        }
    }

    public class SyncManager
    {

        private const string SyncStateFolderName = "syncstate";

        private static readonly Dictionary<string, SyncManager> _runningManagers = new Dictionary<string, SyncManager>(10);

        public static SyncManager ManagerById(string id)
        {
            SyncManager manager;
            if (_runningManagers.TryGetValue(id, out manager))
            {
                return manager;
            }
            return null;
        }


        private IFileManager _fileManager;
        private readonly ILoginService _loginService;
        private bool _runningSync = false;
        private readonly object locker = new object();
        private readonly CancellationTokenSource cts = new CancellationTokenSource();
        private readonly Queue<SyncItem> queue = new Queue<SyncItem>(1000);

        private readonly SyncState _state;
        private readonly IDatabaseService _databaseService;
        private readonly IEncryptionService _encryptionService;
        private readonly IMvxFileStore _fileStore;
        private readonly IMvxMessenger _messenger;
		private readonly IPlatformFileSystem _separator;

        public string Id { get; set; }

        public SyncManager(string id, IFileManager manager) : this(id, manager,null)
        {

        }

        public SyncManager(SyncState state) : this(state.Id,null,state)
        {
            _fileManager = FileManagerBundle.From(state.FileManagerBundle);
        }

        private SyncManager(string id, IFileManager manager, SyncState state = null)
        {
            Id = id;
            
            _fileManager = manager;
            _fileStore = Mvx.Resolve<IMvxFileStore>();
            _messenger = Mvx.Resolve<IMvxMessenger>();
            _encryptionService = Mvx.Resolve<IEncryptionService>();
            _databaseService = Mvx.Resolve<IDatabaseService>();
            _loginService = Mvx.Resolve<ILoginService>();
			_separator = Mvx.Resolve<IPlatformFileSystem>();

            if (state == null)
            {
                _state = new SyncState()
                {
                    Queue = queue,
                    Id = id,
                    FileManagerBundle = FileManagerBundle.Generate(manager)
                };
            }
            else
            {
                _state = state;
                queue = state.Queue;
                if (_state.CurrentItem != null)
                {
                    queue.Enqueue(state.CurrentItem);
                }
            }
        }

        

        public static void Resume(ILoginService loginService)
        {
            try
            {
                IMvxFileStore fileStore = Mvx.Resolve<IMvxFileStore>();

				IPlatformFileSystem separator = Mvx.Resolve<IPlatformFileSystem>();

				string folderPath = ToPath("", separator.Slash,loginService.CompanyHash, SyncStateFolderName);
                fileStore.EnsureFolderExists(folderPath);
                IEnumerable<string> files =
                    fileStore.GetFilesIn(folderPath);

                foreach (string fileName in files)
                {
                    try
                    {
                        SyncState syncState = Settings.LoadFromJson<SyncState>(folderPath + fileName,
                            loginService.UserPasswordHash);
                        SyncManager syncManager = new SyncManager(syncState);
                        syncManager.StartSync();
  
                    }
                    catch (Exception ex)
                    {
                        //sync file is corrupted
                        fileStore.DeleteFile(folderPath+fileName);
                        Debug.WriteLine("Sync file is corrupted, removing");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("!!!!ERROR!!!");
                Debug.WriteLine("FAILED TO RESUME SYNC!");
                Debug.WriteLine(ex.StackTrace);
            }
        }

        public void Cancel()
        {
            cts.Cancel(true);
        }

        public void Sync(FileItem[] pathItems, FileItem item)
        {
            queue.Enqueue(new SyncItem(pathItems, item));
            StartSync();
        }

        public void StartSync()
        {
            lock (locker)
            {
                if (!_runningSync)
                {
                    _runningSync = true;
                    _runningManagers.Add(Id,this);
                    Task.Run(async () =>
                    {
                        try
                        {
                            while (queue.Count > 0)
                            {
                                var test = 1;
                                LinkedList<FileItem> children = null;
                                
                                SyncItem syncItem = queue.Dequeue();
                                _messenger.Publish(new SyncMessage(this, false, syncItem.Item.Name, syncItem.Item.Id));
                                _state.CurrentItem = syncItem;
                                SaveSyncState();
                                OrderedHashSet<int> folderIds = _databaseService.CreateFolders(syncItem.PathItems, syncItem.Item);
                                CheckCanceled();
                                int childAddCount = 0;
                                if (_state.Children != null && _state.Children.Count > 0)
                                {
                                    children = _state.Children;
                                    childAddCount = _state.ChildrenAdded;
                                }
                                else {
                                    _state.ChildrenAdded = 0;
                                    if (syncItem.Item.IsDirectory)
                                    {
                                        children = new LinkedList<FileItem>(await _fileManager.GetChildrenAsync(syncItem.Item));
                                        syncItem.Item.ChildCount = children.Count;
                                    }
                                    else
                                    {
                                        children = new LinkedList<FileItem>();
                                        children.AddLast(syncItem.Item);
                                    }
                                    _state.Children = children;
                                }
                                int initChildCount = children.Count;                   
                                CheckCanceled();
                                bool hasSubDirectories = false;
                                while (children.Count > 0)
                                {
                                    var item = children.First.Value;
                                    if (item.IsDirectory)
                                    {
                                        hasSubDirectories = true;
                                        FileItem[] childPath = new FileItem[syncItem.PathItems.Length + 1];
                                        Array.Copy(syncItem.PathItems, childPath, syncItem.PathItems.Length);
                                        childPath[childPath.Length - 1] = syncItem.Item;
                                        queue.Enqueue(new SyncItem(childPath, item));
                                        _state.Queue = queue;
                                    }
                                    else
                                    {
                                        await DownloadFile(item, folderIds.Last());
                                        _state.ChildrenAdded++;
                                    }
                                    //check if we need to set sync status for the last item
                                    if (children.Count == 1 && !hasSubDirectories)
                                    {
                                        int[] childCountPerFolder = new int[syncItem.PathItems.Length+1];
                                        int i = 0;
                                        foreach (var folder in syncItem.PathItems)
                                        {
                                            childCountPerFolder[i] = folder.ChildCount;
                                            i++;
                                        }

                                        childCountPerFolder[childCountPerFolder.Length - 1] = initChildCount +
                                                                                              childAddCount;
                                        _databaseService.UpdateSyncFlag(folderIds, childCountPerFolder);
                                    }
                                    children.RemoveFirst();
                                    SaveSyncState();
                                }
                                _state.CurrentItem = null;
                            }
                            _messenger.Publish(new SyncMessage(this, true, "", ""));
                            _runningSync = false;
								Settings.Clear(ToPath(Id + ".dat", _separator.Slash, _loginService.CompanyHash, SyncStateFolderName), _loginService.CompanyHash);
                            _runningManagers.Remove(Id);
                        }
                        catch (OperationCanceledException ex)
                        {
								Settings.Clear(ToPath(Id+".dat", _separator.Slash,_loginService.CompanyHash,SyncStateFolderName), _loginService.CompanyHash);
                            queue.Clear();
                            _runningSync = false;
                            //_runningManagers.Remove(Id);
                        }
                        catch (Exception ex)
                        {
								//Settings.Clear(ToPath(Id + ".dat", _separator.Slash, _loginService.CompanyHash, SyncStateFolderName), _loginService.CompanyHash);
                            //queue.Clear();
                            _runningSync = false;
                            _messenger.Publish(new SyncMessage(this, ex));
                            //_runningManagers.Remove(Id);
                        }
                    });
                }
            }
        }

		private static string ToPath(string name, string pathSeperator, params string[] path)
        {
			return path.Aggregate("", (current, part) => current + (part+pathSeperator)) + name;
        }

        private void SaveSyncState()
        {
			Settings.SaveAsJson(_state, ToPath(Id+".dat", _separator.Slash,_loginService.CompanyHash,SyncStateFolderName),  _loginService.UserPasswordHash);    
        }

        private async Task DownloadFile(FileItem item, int folderId)
        {
            CmisFile file = (CmisFile)item.Object;
            Debug.WriteLine("Downloading file=========");
            Debug.WriteLine(file.Name);
            string fileHash = _encryptionService.Sha256(item.Id);
			string localFolder = _loginService.CompanyHash + _separator.Slash + "files" + _separator.Slash;
            string fullPath = localFolder + fileHash;

            //TODO REMOVE JUST FOR DEUGGING
            //await Task.Delay(2000);
            _messenger.Publish(new SyncMessage(this, false, item.Name, item.Id, 0));

            if (!_fileStore.Exists(fullPath) ||
                _databaseService.ItemLastModified(item.Id).Ticks < item.LastModified.Ticks)
            {

                Stream stream =
                    await
                        _fileManager.GetContentStream(item, cts.Token,
                            i =>
                            {
                                _messenger.Publish(new SyncMessage(this,false,item.Name,item.Id,i));
                            });
                MemoryStream memoryStream;
                if (stream is MemoryStream)
                {
                    memoryStream = (MemoryStream) stream;
                }
                else
                {
                    memoryStream = new MemoryStream();
                    await stream.CopyToAsync(memoryStream);
                }
                _fileStore.EnsureFolderExists(localFolder);
                _fileStore.WriteFile(fullPath,
                    _encryptionService.Encrypt(memoryStream.ToArray(), _loginService.CompanyHash));             
            }
            _databaseService.Store(item, fileHash, folderId);
        }

        private void CheckCanceled()
        {
            if (cts.IsCancellationRequested)
            {
                throw new OperationCanceledException();
            }
        }
    }
}
