﻿using System.Threading.Tasks;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileDocs.Core.Net;

namespace MobileDocs.Core.Services
{
    public class CompanyService : ICompanyService
    {
        public async Task<Company> CreateNewCompany(string id)
        {
		
//
//            Company company = await new CompanyTransaction(id).ExecuteAsync<Company>();;
//            if (company == null)
//            {
//                throw new Exception("Profile Id not found");
//            }
//            return company;

            //


			Company company = null;


            await Task.Run(async () =>
            {
                id = id.ToLower();
                await Task.Delay(200);
                if (id == "cgi")
                {
                    company = new Company()
                    {
                        Name = "CGI",
                        Url = "http://www.cgilabs.se/mobiledocs/",
                        Id = "224466"
                    };
                }
                else if (id == "lfv")
                {
                    company = new Company()
                    {
                        Name = "LFV",
						Url = "http://89.221.255.20:8080/md",
                        Id = "123456"
                    };
                }
                else if (id == "vstad")
                {
                    company = new Company()
                    {
                        Name = "Västerrås Stad",
							Url = "http://89.221.255.20:8080/md-vstad",
                        Id = "111222"
                    };
                }
                else
                {
                    throw new Exception("Profile Id not found");
                }
            });
            return company;
        }
    }
}
