﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cirrious.CrossCore.Core;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;
using MobileDocs.Core.Common;
using MobileDocs.Core.Database;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;

namespace MobileDocs.Core.Services
{
    public class DatabaseService : IDatabaseService
    {
        public Item RootItem { get; set; }

        private ISQLiteConnection _connection;
        private readonly ISQLiteConnectionFactory _factory;

        public DatabaseService(ISQLiteConnectionFactory factory)
        {
            _factory = factory;
        }

        public void Init(string dbName)
        {
            _connection = _factory.Create(dbName + ".sql");
            _connection.CreateTable<Item>();
            _connection.CreateTable<Favorite>();
            _connection.CreateTable<RecentFile>();

            RootItem = _connection.Find<Item>(x => x.ParentId == -1);
            if (RootItem == null)
            {
                RootItem = new Item() {ParentId = -1, Name = "root", IsDirectory = true};
                _connection.Insert(RootItem);
                RootItem.Id = LastInsertedId<Item>();
            }
        }

        private int LastInsertedId<T>()
        {
            return _connection.ExecuteScalar<int>("SELECT last_insert_rowid() FROM " + typeof (T).Name);
        }


        public List<Item> Items(int parentId)
        {

            var items = _connection.Table<Item>().Where(x => x.ParentId == parentId).OrderBy(x => x.Name).ToList();
            var favorites = new Dictionary<int,Favorite>(_connection.Table<Favorite>().ToDictionary(x => x.ItemId));
            foreach (var item in items)
            {
                item.IsFavorite = favorites.ContainsKey(item.Id);
            }
            return items;
        }

        public DateTime ItemLastModified(string externalId)
        {
            Item item = _connection.Find<Item>(x => x.ExternalId == externalId);
            if (item == null)
            {
                return default(DateTime);
            }
            return item.LastModified;


        }


        public FileItem ExternalToId(string externalId)
        {
            Item item = _connection.Find<Item>(x => x.ExternalId == externalId);
            if (item == null)
            {
                return null;
            }
            return new LocalFileItem(item);
        }


        public bool IsFavorite(string externalId)
        {
            Item item = _connection.Find<Item>(x => x.ExternalId == externalId);
            if (item != null)
            {
                Favorite favorite = _connection.Find<Favorite>(x => x.ItemId == item.Id);
                if (favorite != null)
                {
                    return true;
                }
                return false;
            }
            return false;
            //TODO impelement proper join
            //var test = _connection
            //    .Table<Item>()
            //    .Where(z => z.ExternalId == externalId)
            //    .Join(_connection.Table<Favorite>(), x => x.Id, y => y.ItemId,
            //        (a, b) => new KeyValuePair<Item, Favorite>(a, b));

            //_connection.Table<Item>().Select(x => x.Id).Join(y => y.)
            //_connection.Find<Item>(x => x.E)
            return true;
        }

        public void AddFavorite(string externalId)
        {
            Item item = _connection.Find<Item>(x => x.ExternalId == externalId);
            if (item != null)
            {
                _connection.Insert(new Favorite()
                {
                    ItemId = item.Id
                });
            }
        }

        public void RemoveFavorite(string externalId)
        {
            Item item = _connection.Find<Item>(x => x.ExternalId == externalId);
            if (item != null)
            {
                _connection.Execute("DELETE FROM " + typeof(Favorite).Name + " WHERE " + GetPropertyName(() => new Favorite().ItemId) + " = " + item.Id);
            }
        }

        public static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            return MvxPropertyNameExtensionMethods.GetPropertyNameFromExpression(null, expression);
        }

        public List<FileItem> RecentFiles()
        {
            List<RecentFile> recentFiles =
                _connection.Table<RecentFile>().OrderByDescending(x => x.DateOpened).ToList();

            int size = recentFiles.Count;
            List<int> recentFileIds = new List<int>(size);
            recentFileIds.AddRange(recentFiles.Select(recentFile => recentFile.ItemId));
            List<Item> items =
                _connection.Table<Item>().Where(y => recentFileIds.Contains(y.Id)).ToList();
            List<FileItem> browseItems = new List<FileItem>(new FileItem[size]);
            Dictionary<int, Favorite> favorites = _connection.Table<Favorite>().ToDictionary(x => x.ItemId);
                
            foreach (var item in items)
            {
                item.IsFavorite = favorites.ContainsKey(item.Id);
                browseItems[recentFileIds.IndexOf(item.Id)] = new LocalFileItem(item);
            }

            return browseItems;
        }

        public List<FileItem> Favorites()
        {
            List<Favorite> favs = _connection.Table<Favorite>().ToList();

            List<FileItem> fileItems = new List<FileItem>(favs.Count);

            int size = favs.Count;
            List<int> favFileIds = new List<int>(size);
            favFileIds.AddRange(favs.Select(favoriteFile => favoriteFile.ItemId));

            List<Item> items =
               _connection.Table<Item>().Where(y => favFileIds.Contains(y.Id)).ToList();

            List<FileItem> browseItems = new List<FileItem>(new FileItem[size]);

            foreach (Item item in items)
            {
                browseItems[favFileIds.IndexOf(item.Id)] = new LocalFileItem(item);
            }

            return browseItems;

        }


        public void AddRecent(FileItem item, int itemId)
        {
            List<RecentFile> recentFiles = _connection.Table<RecentFile>().OrderBy(x => x.DateOpened).ToList();
            if (recentFiles.Count > 10)
            {
                _connection.Delete<RecentFile>(recentFiles.First().Id);
            }
            RecentFile recent = _connection.Find<RecentFile>(x => x.ItemId == itemId);
            if (recent != null)
            {
                _connection.Delete<RecentFile>(recent.Id);
            }
            _connection.Insert(new RecentFile()
            {
                ItemId = itemId,
                DateOpened = DateTime.Now
            });
        }

        private int CreateFolder(FileItem folder, int parentId)
        {
            Item folderItem =
                   _connection.Find<Item>(
                       x => x.IsDirectory == true && x.ParentId == parentId && x.Name == folder.Name);
            if (folderItem != null)
            {
                return folderItem.Id;
            }
            else
            {
                folderItem = new Item()
                {
                    ParentId = parentId,
                    Name = folder.Name,
                    IsDirectory = true,
                    LastModified = folder.LastModified,
                    ExternalId = folder.Id
                };
                _connection.Insert(folderItem);
                return folderItem.Id;   
            }
        }


        public OrderedHashSet<int> CreateFolders(FileItem[] folderPath, FileItem currentItem)
        {

            OrderedHashSet<int> folderIds = new OrderedHashSet<int>();

            _connection.BeginTransaction();
            

            //remove root entry
            int copyDelta = 1;
            if (currentItem.IsDirectory)
            {
                copyDelta = 0;
            }
            FileItem[] newPath = new FileItem[folderPath.Length - copyDelta];
            Array.Copy(folderPath, 1, newPath, 0, folderPath.Length - 1);
            if (currentItem.IsDirectory)
            {
                newPath[newPath.Length-1] = currentItem;
            }


            Item parentFolder = RootItem;
            int parentId = RootItem.Id;

            foreach (FileItem pathItem in newPath)
            {
                folderIds.Add(parentId);
                parentId = CreateFolder(pathItem, parentId);
            }
            folderIds.Add(parentId);
            _connection.Commit();
            return folderIds;
        }


        public void UpdateSyncFlag(OrderedHashSet<int> folderIds, int[] childCountPerFolder)
        {
            _connection.BeginTransaction();
            List<Item> folders = _connection.Table<Item>().Where(x => folderIds.Contains(x.Id)).ToList();
            Item[] orderedItems = new Item[folderIds.Count];
            foreach (var item in folders)
            {
                orderedItems[folderIds.IndexOf(item.Id)] = item;
            }

            for (int i = orderedItems.Length - 1; i > -1; i--)
            {
                Item item = orderedItems[i];
                int localChildCount = childCountPerFolder[i];
                int dbChildCount = _connection.Table<Item>().Count(x => x.ParentId == item.Id);

                if (localChildCount == dbChildCount)
                {
                    item.IsSynchronized = true;
                    _connection.Update(item);
                }
                else
                {
                    break;
                }

            }
            _connection.Commit();
        }


        public void Store(FileItem item, string assetId, int parentId)
        {
            _connection.BeginTransaction();
            Item newItem = new Item()
            {
                Name = item.Name,
                LastModified = item.LastModified,
                ParentId = parentId,
                ExternalId = item.Id,
            };

            if (item.IsDirectory)
            {
                newItem.IsDirectory = true;
            }
            else
            {
                newItem.AssetId = assetId;
                newItem.Size = (long)item.Size;
                newItem.IsSynchronized = true;
            }


            int lastInsertedItemId = -1;

            Item existingItem = _connection.Find<Item>(x => x.ExternalId == item.Id);
            if (existingItem == null)
            {
                _connection.Insert(newItem);
            }
            else
            {
                newItem.Id = existingItem.Id;
                _connection.Update(newItem);
            }
            _connection.Commit();
        }


        public bool HasFavorites()
        {
            return _connection.Table<Favorite>().Any();
        }
    }
}
