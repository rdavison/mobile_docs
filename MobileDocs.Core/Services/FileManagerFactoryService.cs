﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileDocs.Core.Cmis;
using MobileDocs.Core.Common;
using MobileDocs.Core.Interfaces;

namespace MobileDocs.Core.Services
{
    public class FileManagerFactoryService : IFileManagerFactoryService
    {

        private readonly ILoginService _loginService;

        public FileManagerFactoryService(ILoginService loginService)
        {
            _loginService = loginService;
        }

        public IFileManager CreateManager(Type type, Dictionary<String, String> bundle)
        {
            if (type == typeof (CmisFileManager))
            {
                return CreateCmisFileManager(bundle);
            }
            if (type == typeof(LocalFileManager))
            {
                return CreateLocalFileManager();
            }
            throw new ArgumentException("No file manager with type \""+type.Name+"\" exists");
        }

        private CmisFileManager CreateCmisFileManager(Dictionary<string, string> bundle)
        {
            return new CmisFileManager(new OpenCmisClient(_loginService.Username, _loginService.Password, bundle["url"]));
        }

        private CmisFileManager CreateLocalFileManager()
        {
            throw new NotImplementedException();
        }
    }
}
