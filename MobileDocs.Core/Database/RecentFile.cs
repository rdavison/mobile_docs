﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;

namespace MobileDocs.Core.Database
{
    public class RecentFile
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Unique]
        public int ItemId { get; set; }
        public DateTime DateOpened { get; set; }
    }
}
