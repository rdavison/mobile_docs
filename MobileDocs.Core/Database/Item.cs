﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;

namespace MobileDocs.Core.Database
{
    public class Item
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDirectory { get; set; }
        public bool IsSynchronized { get; set; }
        [Unique]
        public string ExternalId { get; set; }
        [Unique]
        public string AssetId { get; set; }
        public int ParentId { get; set; }
        public DateTime LastModified { get; set; }
        public long Size { get; set; }
        [Ignore]
        public bool IsFavorite { get; set; }
        
    }
}
