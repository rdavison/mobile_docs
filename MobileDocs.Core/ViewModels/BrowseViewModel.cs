﻿using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Core;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using MobileDocs.Core.Cmis;
using MobileDocs.Core.Common;
using MobileDocs.Core.Database;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using MobileDocs.Core.Services;
using Newtonsoft.Json;

namespace MobileDocs.Core.ViewModels
{
    public class BrowseParameters
    {
        public BrowseType Type { get; set; }
        public string RepoUrl { get; set; }
        public string RepoName { get; set; }
    }

    public enum BrowseType
    {
        Local, Remote
    }

    public class RootFile : FileItem
    {
        private string _name;
        public RootFile(string name)
        {
            this._name = name;
        }

        public override string Name { get { return _name; } }
        public override string Path { get { return "/"; } }
        public override ulong Size { get; set; }
        public override object Object { get; set; }
        public override string Id { get { return "root"; } }
        public override bool IsDirectory {get { return true; }}
        public override DateTime LastModified { get; set; }
        public override bool IsSynchronized { get; set; }
        public override bool IsFavorite { get; set; }
    }



    public class BrowseViewModel : BaseViewModel
    {

        private bool _fromBundle = false;
        private FileItem root = new RootFile("root");
        private SyncManager _syncManager;
        private FileItem _currentFolder;
        private IFileManager _manager;
        private IFileManager _localManager;
        private ObservableCollection<FileItem> _fetchedItems;
        private CancellationTokenSource _cancellationTokenSource;
        private readonly ILoginService _loginService;
        private readonly IMvxFileStore _fileStore;
        private readonly IEncryptionService _encryptionService;
        private readonly IDatabaseService _databaseService;
        private bool _ignoreFilter;
        private readonly MvxSubscriptionToken _messageToken;
        private string _startPath;

		private readonly IPlatformFileSystem _separator;

		public BrowseViewModel(IMvxMessenger messenger,IDatabaseService databaseService, ILoginService loginService, IMvxFileStore fileStore, IEncryptionService encryptionService, IPlatformFileSystem separator)
        {
            _loginService = loginService;
            _fileStore = fileStore;
            _encryptionService = encryptionService;
            _databaseService = databaseService;
			_separator = separator;

            _messageToken = messenger.SubscribeOnMainThread<SyncMessage>(OnSyncMessage);

            SortOptions.Add(new SortOption("Sort by name", SortBy.Name));
            SortOptions.Add(new SortOption("Sort by date", SortBy.Date));
            SortOptions.Add(new SortOption("Sort by size", SortBy.Size));

            SelectedSortIndex = Settings.GetInt(Settings.KeySelectedSortIndex);
            _cancellationTokenSource = new CancellationTokenSource();
        }

        private void OnSyncMessage(SyncMessage obj)
        {
            if (obj.Exception != null)
            {
                SyncFailed = true;
                SyncStateText = "Sync failed!";
            }
            else
            {
                if (obj.IsComplete)
                {
                    SyncFileName = "";
                    SyncFileProgress = null;
                    //int i = 0;
                    //foreach (var itm in Items)
                    //{
                    //    itm.IsSynchronized = true;
                    //    Items[i] = itm;
                    //    i++;
                    //}
                }
                else
                {
                    SyncFileName = obj.CurrentFileName;
                    SyncFileProgress = obj.Progress;
                }
                SyncRunning = !obj.IsComplete;
            }

            

            ////update item to synced
            //int i = 0;
            //foreach (var itm in Items)
            //{
            //    if (itm.Id == obj.CurrentFileId)
            //    {
            //        itm.IsSynchronized = true;
            //        Items[i] = itm;
            //        break;
            //    }
            //    i++;
            //}
        }




        private int _selectedSortIndex;
        public int SelectedSortIndex
        {
            get { return _selectedSortIndex; }
            set {
                _selectedSortIndex = value;
                RaisePropertyChanged(() => SelectedSortIndex);
                Items = Sort(SortOptions[value].SortBy, Items.ToList());
                Settings.Save(Settings.KeySelectedSortIndex,value);
            }
        }

        private ObservableCollection<FileItem> Sort(SortBy sortBy, List<FileItem> list)
        {
            switch (sortBy)
            {
                case SortBy.Name:
                    list = (from i in list
                            orderby !i.IsDirectory, i.Name
                            select i).ToList();
                    break;
                case SortBy.Date:
                    list = (from i in list
                            orderby !i.IsDirectory, i.LastModified
                            select i).ToList();
                    break;
                case SortBy.Size:
                    list = (from i in list
                            orderby !i.IsDirectory, i.Size
                            select i).ToList();
                    break;
            }

            return new ObservableCollection<FileItem>(list);
        }


        private string _filterText;
        public string FilterText
        {
            get { return _filterText; }
            set
            {
                _filterText = value;
                RaisePropertyChanged(() => FilterText);
                if (!_ignoreFilter)
                {
                    List<FileItem> filterItems = new List<FileItem>(_fetchedItems);
                    filterItems = filterItems.FindAll(x => x.Name.ToLower().Contains(value.ToLower()));
                    Items = Sort(SortOptions[SelectedSortIndex].SortBy, filterItems);
                }
                else
                {
                    _ignoreFilter = false;
                }
                
            }
        }


        private bool _syncEnabled;
        public bool SyncEnabled
        {
            get { return _syncEnabled; }
            set { _syncEnabled = value; RaisePropertyChanged(() => SyncEnabled); }
        }
        



        private ObservableCollection<SortOption> _sortOptions = new ObservableCollection<SortOption>();
        public ObservableCollection<SortOption> SortOptions
        {
            get { return _sortOptions; }
            set { _sortOptions = value; RaisePropertyChanged(() => SortOptions); }
        }

        public async override void Start()
        {

            if (!_fromBundle)
            {
                Paths.Add(root);
                await ShowRoot();
            }
            base.Start();
        }

        public async void Init(BrowseParameters browseParameters)
        {
			LoadingText = "TEST";
            
            _localManager = new LocalFileManager(_databaseService);
            switch (browseParameters.Type)
            {
                case BrowseType.Local:
                    _manager = _localManager;
                    SyncEnabled = false;
                    break;
                case BrowseType.Remote:
                    SyncEnabled = true;
                    _startPath = browseParameters.RepoName;
                    _manager =  new CmisFileManager(new OpenCmisClient(_loginService.Username, _loginService.Password,
                            browseParameters.RepoUrl));

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            if (_manager is CmisFileManager)
            {
                string syncId = _encryptionService.Sha256(browseParameters.RepoUrl);
                _syncManager = SyncManager.ManagerById(syncId);
                if (_syncManager == null)
                {
                    //no running manager, instanciate a new one
                    _syncManager = new SyncManager(syncId, _manager);
                }
            }
        }

        private void HandleException(Exception ex)
        {
            IsLoading = false;
            if (ex is HttpRequestException)
            {
                if (ex.Message.Contains("403"))
                {
                    ReportError("You are not authorized and have now been loged out");
                    _loginService.Logout();
                    ShowViewModelAndClearBackStack<LoginViewModel>();
                    //Close(this);
                }
                else
                {
                    ReportError("Something went wrong with request - " + ex.Message);
                }
            }
            else
            {
                ReportError("Ops, something went wrong - " + ex.Message);
            }
        }

        /// <summary>
        /// Looks up the folder if it exists in the local db. If so, we add some props from the the local db
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        private async Task<List<FileItem>>  SetSyncedProps(List<FileItem> items)
        {
            if (!(_manager is LocalFileManager))
            {
                Dictionary<string, FileItem> localItems = new Dictionary<string, FileItem>(0);
                FileItem localFolder = _databaseService.ExternalToId(_currentFolder.Id);
                if (localFolder != null)
                {
                   localItems =
                           (await _localManager.GetChildrenAsync(localFolder)).ToDictionary(
                               x => ((Item)x.Object).ExternalId);
                }
                foreach (FileItem item in items)
                {
                    FileItem localItem = null;
                    if (localItems.TryGetValue(item.Id, out localItem))
                    {
                        item.IsSynchronized = localItem.IsSynchronized;
                        item.IsFavorite = localItem.IsFavorite;
                    }
                }
                

                //Dictionary<string, FileItem> localItems = (await _localManager.GetRoot()).ToDictionary(x => x.Id);
                //foreach (var item in _fetchedItems)
                //{
                //    FileItem localItem = null;
                //    if (localItems.TryGetValue(item.Id, out localItem))
                //    {
                //        item.IsSynchronized = localItem.IsSynchronized;
                //        item.IsFavorite = localItem.IsFavorite;
                //    }
                //}
            }
            return items;
        }

        private async Task ShowRoot()
        {
            IsLoading = true;
            PageTitle = root.Name;
            _currentFolder = root;
            try
            {
                List<FileItem> items = await _manager.GetRoot();
                items = await SetSyncedProps(items);
                _fetchedItems = new ObservableCollection<FileItem>(items);          
                root.ChildCount = items.Count;
                FileItem subFolder = items.Find(x => x.Name == _startPath);
                if (subFolder == null)
                {
                    Items = Sort(SortOptions[SelectedSortIndex].SortBy, items);
                    IsLoading = false;
                }
                else
                {
                    _startPath = null;
                    Paths.Add(subFolder);
                    BrowseTo(subFolder);
                }
                
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            
            
        }

        public async void BrowseTo(FileItem folder)
        {
            IsLoading = true;
            try
            {
                Items.Clear();
                _ignoreFilter = true;
                FilterText = "";
                PageTitle = folder.Name;
                _currentFolder = folder;
                List<FileItem> items = await _manager.GetChildrenAsync(folder);             
                folder.ChildCount = items.Count;
                bool isLocal = _manager is LocalFileManager;
                items = await SetSyncedProps(items);
                Items = Sort(SortOptions[SelectedSortIndex].SortBy, items);
                _fetchedItems = new ObservableCollection<FileItem>(items);
                IsLoading = false;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }


        private ObservableCollection<FileItem> _items = new ObservableCollection<FileItem>();
        public ObservableCollection<FileItem> Items
        {
            get { return _items; }
            set { _items = value; RaisePropertyChanged(() => Items); }
        }

        private ObservableCollection<FileItem> _paths = new ObservableCollection<FileItem>();
        public ObservableCollection<FileItem> Paths
        {
            get { return _paths; }
            set { _paths = value; RaisePropertyChanged(() => Paths); }
        }


        private bool _isLoadingDocument;
        public bool IsLoadingDocument
        {
            get { return _isLoadingDocument; }
            set { _isLoadingDocument = value; RaisePropertyChanged(() => IsLoadingDocument); }
        }


        private string _loadingText;
        public string LoadingText
        {
            get { return _loadingText; }
            set { _loadingText = value; RaisePropertyChanged(() => LoadingText); }
        }

        private string _pageTitle;
        public string PageTitle
        {
            get { return _pageTitle; }
            set { _pageTitle = value; RaisePropertyChanged(() => PageTitle); }
        }


        private MvxCommand<FileItem> _navigatePathCommand;
        public ICommand NavigatePathCommand
        {
            get
            {
                _navigatePathCommand = _navigatePathCommand ?? new MvxCommand<FileItem>(NavigatePath);
                return _navigatePathCommand;
            }
        }

        public async void NavigatePath(FileItem item)
        {
   
                int size = Paths.Count;
                int index = Paths.IndexOf(item);
                for (int i = size - 1; i > index; i--)
                {
                    Paths.RemoveAt(i);
                }
                if (item is RootFile)
                {
                    await ShowRoot();
                }
                else
                {
                    BrowseTo(item);
                }
           
        }


        private ObservableCollection<FileItem> _selectedItems;
        public ObservableCollection<FileItem> SelectedItems
        {
            get { return _selectedItems; }
            set { _selectedItems = value; RaisePropertyChanged(() => SelectedItems); }
        }
        




        private MvxCommand<List<object>> _selectionChangedCommand;
        public ICommand SelectionChangedCommand
        {
            get
            {
                _selectionChangedCommand = _selectionChangedCommand ?? new MvxCommand<List<object>>(SelectionChanged);
                return _selectionChangedCommand;
            }
        }

        public void SelectionChanged(List<object> objects)
        {
            SelectedItems = new ObservableCollection<FileItem>(objects.Cast<FileItem>());
        }


        private bool _syncRunning = false;
        public bool SyncRunning
        {
            get { return _syncRunning; }
            set { _syncRunning = value; RaisePropertyChanged(() => SyncRunning); }
        }


        private int? _syncFileProgress;
        public int? SyncFileProgress
        {
            get { return _syncFileProgress; }
            set { _syncFileProgress = value; RaisePropertyChanged(() => SyncFileProgress); }
        }


        private string _syncFileName;
        public string SyncFileName
        {
            get { return _syncFileName; }
            set { _syncFileName = value; RaisePropertyChanged(() => SyncFileName); }
        }



        private MvxCommand<FileItem> _clickItemCommand;
        public ICommand ClickItemCommand
        {
            get
            {
                _clickItemCommand = _clickItemCommand ?? new MvxCommand<FileItem>(ClickItem);
                return _clickItemCommand;
            }
        }


        private MvxCommand _selectItemCommand;
        public ICommand SelectItemCommand
        {
            get
            {
                _selectItemCommand = _selectItemCommand ?? new MvxCommand(SelectItem);
                return _selectItemCommand;
            }
        }

        private void SelectItem()
        {
            
        }


        private string _syncStateText;
        public string SyncStateText
        {
            get { return _syncStateText; }
            set { _syncStateText = value; RaisePropertyChanged(() => SyncStateText); }
        }
        





        public async void ClickItem(FileItem item)
        {
            if (item.IsDirectory)
            {
                Paths.Add(item);
                BrowseTo(item);
            }
            else
            {      
                ShowDocument(item);
            }
        }

        private async void ShowDocument(FileItem item)
        {
            try
            {
                _cancellationTokenSource = new CancellationTokenSource();
                DocumentParameters docParams = new DocumentParameters() {Name = item.Name, Id = item.Id, FileManagerBundleJson = FileManagerBundle.Generate(_manager).ToJson()};
                string fileHash = _encryptionService.Sha256(item.Id);
				string localFolder = _loginService.CompanyHash + _separator.Slash + "files" + _separator.Slash;
                string fullPath = localFolder + fileHash;
                int dbId = -1;
                int.TryParse(item.Id, out dbId);
                if (item is RemoteFileItem)
                {
                    if (!_fileStore.Exists(fullPath) ||
                        _databaseService.ItemLastModified(item.Id).Ticks < item.LastModified.Ticks)
                    {
                        IsLoadingDocument = true;
                        LoadingText = "Downloading document";
                        LoadingProgress = 0;

                        Stream stream =
                            await
                                _manager.GetContentStream(item, _cancellationTokenSource.Token,
                                    i => InvokeOnMainThread(() => LoadingProgress = i));
                        MemoryStream memoryStream;
                        if (stream is MemoryStream)
                        {
                            memoryStream = (MemoryStream) stream;
                        }
                        else
                        {
                            memoryStream = new MemoryStream();
                            await stream.CopyToAsync(memoryStream);
                        }
                        _fileStore.EnsureFolderExists(localFolder);
                        _fileStore.WriteFile(fullPath,
                            _encryptionService.Encrypt(memoryStream.ToArray(), _loginService.CompanyHash));
                        //dbId = _databaseService.Store(Paths.ToArray(), item, TypeEnum.File, fileHash);
                    }
                }
                FileItem localItem = _databaseService.ExternalToId(item.Id);
                if (localItem != null)
                {
                    dbId = ((Item)localItem.Object).Id;
                    _databaseService.AddRecent(item, dbId);
                    docParams.IsSynchronized = true;
                }
                docParams.Path = fullPath;
                docParams.IsFavorite = _databaseService.IsFavorite(item.Id);
                IsLoadingDocument = false;
                ShowViewModel<DocumentViewModel>(docParams);
            }
            catch (OperationCanceledException ex)
            {
                LoadingProgress = 0;
                IsLoadingDocument = false;
            }
            catch (Exception ex)
            {
                LoadingProgress = 0;
                IsLoadingDocument = false;
                HandleException(ex);
            }
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {

                FileItem item = Paths[0];
                string jsonPaths = JsonConvert.SerializeObject(Paths, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects,
                    TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
                });
                bundle.Data["paths"] = jsonPaths;
            
        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            _fromBundle = true;
            Paths = JsonConvert.DeserializeObject<ObservableCollection<FileItem>>(state.Data["paths"], new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects
            });
            BrowseTo(Paths.Last());
        }



        private MvxCommand _goUpCommand;
        public ICommand GoUpCommand
        {
            get
            {
                _goUpCommand = _goUpCommand ?? new MvxCommand(GoUp);
                return _goUpCommand;
            }
        }

        public async void GoUp()
        {
            if (_currentFolder == root)
            {
                return;
            }
            FileItem parent = Paths[Paths.Count - 2];   
            if (parent is RootFile)
            {
                Paths.RemoveAt(Paths.Count - 1);
                await ShowRoot();
            }
            else {
                Paths.RemoveAt(Paths.Count - 1);
                BrowseTo(parent);
            }
        }

        private MvxCommand _goBackCommand;
        public ICommand GoBackCommand
        {
            get
            {
                _goBackCommand = _goBackCommand ?? new MvxCommand(() => Close(this));
                return _goBackCommand;
            }
        }


        private MvxCommand _syncCommand;
        public ICommand SyncCommand
        {
            get
            {
                _syncCommand = _syncCommand ?? new MvxCommand(Sync);
                return _syncCommand;
            }
        }

        public class TestClass
        {
            public string Name { get; set; }
            public string Url { get; set; }
        }

        private void Sync()
        {
            SyncRunning = true;
            SyncFailed = false;
            SyncStateText = "Synchronizing";
            foreach (FileItem item in SelectedItems)
            {
                _syncManager.Sync(Paths.ToArray(), item);
            }
            SelectedItems = null;
        }



        private MvxCommand _cancelLoadDoumentCommand;
        public ICommand CancelLoadDoumentCommand
        {
            get
            {
                _cancelLoadDoumentCommand = _cancelLoadDoumentCommand ?? new MvxCommand(
                    () =>
                    {
                        _cancellationTokenSource.Cancel(true);
                        IsLoadingDocument = false;
                    });
                return _cancelLoadDoumentCommand;
            }
        }



        private int? _loadingProgress;
        public int? LoadingProgress
        {
            get { return _loadingProgress; }
            set { _loadingProgress = value; RaisePropertyChanged(() => LoadingProgress); }
        }


        private bool _syncFailed;
        public bool SyncFailed
        {
            get { return _syncFailed; }
            set { _syncFailed = value; RaisePropertyChanged(() => SyncFailed); }
        }


        private MvxCommand _retrySyncCommand;
        public ICommand RetrySyncCommand
        {
            get
            {
                _retrySyncCommand = _retrySyncCommand ?? new MvxCommand(RetrySync);
                return _retrySyncCommand;
            }
        }

        public void RetrySync()
        {
            SyncStateText = "Synchronizing";
            SyncFailed = false;
            _syncManager.StartSync();
        }

        

        


    }
}
