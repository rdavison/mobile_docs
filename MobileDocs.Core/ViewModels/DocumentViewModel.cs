﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Cirrious.CrossCore.Plugins;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using MobileDocs.Core.Common;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Services;
using Newtonsoft.Json;

namespace MobileDocs.Core.ViewModels
{

    public class DocumentMessage : MvxMessage
    {
        public DocumentMessage(object sender, byte[] bytes, string fileExtention)
            : base(sender)
        {
            Bytes = bytes;
            FileExtention = fileExtention;
        }

        public string FileExtention { get; private set; }
        public byte[] Bytes { get; private set; }
    }

    public class DocumentParameters
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Path { get; set; }
        public bool IsFavorite { get; set; }
        public bool IsSynchronized { get; set; }
        public string FileManagerBundleJson { get; set; }
    }

    public class DocumentViewModel : BaseViewModel
    {

        private const String IconFavorite = "\uE113";
        private const String IconUnfavorite = "\uE195";
        private const String IconSynced = "\uE1df";
        private const String IconUnsynced = "\uE1dd";

        private readonly IMvxFileStore _fileStore;
        private readonly IMvxMessenger _messenger;
        private readonly IEncryptionService _encryptionService;
        private readonly ILoginService _loginService;
        private readonly IDatabaseService _databaseService;
        private string _fileExtention;
        private IFileManager _fileManager;

        private string _externalId;
        private bool _isFavorite;


        public DocumentViewModel(IDatabaseService databaseService, ILoginService loginService, IEncryptionService encryptionService, IMvxFileStore fileStore, IMvxMessenger messenger)
        {
            _fileStore = fileStore;
            _messenger = messenger;
            _encryptionService = encryptionService;
            _loginService = loginService;
            _databaseService = databaseService;
        }

        public override async void Start()
        {
            base.Start();
            await Task.Delay(2000);
            MenuActive = false;
           
        }

        public async void Init(DocumentParameters initParams)
        {
            DocumentTitle = initParams.Name;
            _externalId = initParams.Id;
            _fileExtention = System.IO.Path.GetExtension(initParams.Name);
            IsLoading = true;
            _isFavorite = initParams.IsFavorite;
            _isSynchronized = initParams.IsSynchronized;



            FavoriteButtonText = _isFavorite ? IconUnfavorite : IconFavorite;
            SyncButtonText = _isSynchronized ? IconUnsynced : IconSynced;

            _fileManager = FileManagerBundle.FromJson(initParams.FileManagerBundleJson);

            
            //LoadingMessage = "Decrypting document...";

            // ReSharper disable once CSharpWarnings::CS4014 we want to show title while loading in background
            Task.Run(async () =>
            {
                _fileStore.TryReadBinaryFile(initParams.Path, ReadMethod);
            });
        }


        private bool _isSynchronized;
        public bool IsSynchronized
        {
            get { return _isSynchronized; }
            set { _isSynchronized = value; RaisePropertyChanged(() => IsSynchronized); }
        }

        private bool _menuActive;
        public bool MenuActive
        {
            get { return _menuActive; }
            set { _menuActive = value; RaisePropertyChanged(() => MenuActive); }
        }
        


        private bool ReadMethod(Stream stream)
        {
            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            byte[] decryped = _encryptionService.Decrypt(ms.ToArray(), _loginService.CompanyHash);
            IsLoading = false;
            _messenger.Publish(new DocumentMessage(this, decryped, _fileExtention));
            return true;
        }


        private string _documentTitle;
        public string DocumentTitle
        {
            get { return _documentTitle; }
            set { _documentTitle = value; RaisePropertyChanged(() => DocumentTitle); }
        }

        private MvxCommand _goBackCommand;
        public ICommand GoBackCommand
        {
            get
            {
                _goBackCommand = _goBackCommand ?? new MvxCommand(() => Close(this));
                return _goBackCommand;
            }
        }


        private string _favoriteButtonText = IconFavorite;
        public string FavoriteButtonText
        {
            get { return _favoriteButtonText; }
            set { _favoriteButtonText = value; RaisePropertyChanged(() => FavoriteButtonText); }
        }



        private string _syncButtonText;
        public string SyncButtonText
        {
            get { return _syncButtonText;; }
            set { _syncButtonText = value; RaisePropertyChanged(() => SyncButtonText); }
        }




        private MvxCommand _toggleSyncCommand;
        public ICommand ToggleSyncCommand
        {
            get
            {
                _toggleSyncCommand = _toggleSyncCommand ?? new MvxCommand(ToggleSync);
                return _toggleSyncCommand;
            }
        }

        private void ToggleSync()
        {
            if (_fileManager is CmisFileManager)
            {

                //SyncManager manager = new SyncManager(new Guid().ToString(), _fileManager);
                //manager.Sync();
            }
        }

        



        private MvxCommand _toggleFavoriteCommand;
        public ICommand ToggleFavoriteCommand
        {
            get
            {
                _toggleFavoriteCommand = _toggleFavoriteCommand ?? new MvxCommand(ToggleFavorite, () => IsSynchronized);
                return _toggleFavoriteCommand;
            }
        }

        public void ToggleFavorite()
        {
            if (_isFavorite)
            {
                _databaseService.RemoveFavorite(_externalId);
                FavoriteButtonText = IconFavorite;
            }
            else
            {
                _databaseService.AddFavorite(_externalId);
                FavoriteButtonText = IconUnfavorite;
            }
        }

        
    }
}
