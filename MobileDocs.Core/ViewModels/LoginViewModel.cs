﻿using System.Diagnostics;
using System.Threading.Tasks;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.MvvmCross.ViewModels;
using MobileDocs.Core.Common;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;
using MobileDocs.Core.Net;
using MobileDocs.Core.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;

//
// TODO Add cancelCommand to add new company dialog.
//

namespace MobileDocs.Core.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {

        

        private readonly ICompanyService _companyService;
        private readonly ILoginService _loginService;
        private readonly IMvxFileStore _filePlugin;
        private readonly IConnectionService _connectionService;
		private readonly IPlatformFileSystem _separator;

        private bool _cancelLogin = false;

		public LoginViewModel(ICompanyService companyService, ILoginService loginService, IMvxFileStore filePlugin, IConnectionService connectionService, IPlatformFileSystem separator)
        {



            this._loginService = loginService;
            this._companyService = companyService;
            this._filePlugin = filePlugin;
            this._connectionService = connectionService;
			this._separator = separator;

            //set the default CGI color on this page
            Theme.AccentColor = "#e31937";


            try
            {
                Company[] loadedComapnies = Settings.LoadFromJson<Company[]>(Settings.PathCompanyData, "");
                if (loadedComapnies != null)
                {
                    var temp = Settings.SelectedCompanyIndex;
                    Companies = new ObservableCollection<Company>(loadedComapnies);
                    SelectedCompanyIndex = temp;
                    SelectedCompany = Companies[temp];
                }

            }
            catch (Exception ex)
            {
                ReportError("Sorry - Could not load profiles " + ex.Message);
            }


        }

        private string _loadingText;
        public string LoadingText
        {
            get { return _loadingText; }
            set { _loadingText = value; RaisePropertyChanged(() => LoadingText); }
        }

        private string _newCompanyStatus;
        public string NewCompanyStatus
        {
            get { return _newCompanyStatus; }
            set { _newCompanyStatus = value; RaisePropertyChanged(() => NewCompanyStatus); }
        }


        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; RaisePropertyChanged(() => Password); _loginCommand.RaiseCanExecuteChanged(); }
        }

        private int _selectedCompanyIndex;
        public int SelectedCompanyIndex
        {
            get { return _selectedCompanyIndex; }
            set
            {
                _selectedCompanyIndex = value;
                RaisePropertyChanged(() => SelectedCompanyIndex);
                Settings.SelectedCompanyIndex = value;
            }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { _username = value; RaisePropertyChanged(() => Username); _loginCommand.RaiseCanExecuteChanged(); }
        }


        private Company _selectedCompany;
        public Company SelectedCompany
        {
            get { return _selectedCompany; }
            set { _selectedCompany = value; RaisePropertyChanged(() => SelectedCompany); }
        }

        private ObservableCollection<Company> _companies = new ObservableCollection<Company>();
        public ObservableCollection<Company> Companies
        {
            get { return _companies; }
            set { _companies = value; RaisePropertyChanged(() => Companies); }
        }


        private bool _showAddCompany;
        public bool ShowAddCompany
        {
            get { return _showAddCompany; }
            set { _showAddCompany = value; 
				RaisePropertyChanged(() => ShowAddCompany); 
			}
        }



        private string _newCompanyId;
        public string NewCompanyId
        {
            get { return _newCompanyId; }
            set { _newCompanyId = value; RaisePropertyChanged(() => NewCompanyId); _addCompanyCommand.RaiseCanExecuteChanged(); }
        }


		private bool _newCompanyLoading;
        public bool NewCompanyLoading
        {
            get { return _newCompanyLoading; }
            set { _newCompanyLoading = value; RaisePropertyChanged(() => NewCompanyLoading); }
        }
        


        //command defintions
        private MvxCommand _cancelLoginCommand;
        public ICommand CancelLoginCommand
        {
            get { _cancelLoginCommand = _cancelLoginCommand ?? new MvxCommand(() =>
            {
                IsLoading = false;
                _cancelLogin = true;
            } );
    return
            _cancelLoginCommand;
            }
        }

        MvxCommand _showAddComanyCommand;
        public System.Windows.Input.ICommand ShowAddCompanyCommand
        {
            get
            {
                _showAddComanyCommand = _showAddComanyCommand ?? new MvxCommand(() => ShowAddCompany = true);
                return _showAddComanyCommand;
            }
        }

        MvxCommand _hideAddComanyCommand;
        public System.Windows.Input.ICommand HideAddCompanyCommand
        {
            get
            {
                _hideAddComanyCommand = _hideAddComanyCommand ?? new MvxCommand(() => ShowAddCompany = false);
                return _hideAddComanyCommand;
            }
        }




        MvxCommand _addCompanyCommand;
        public System.Windows.Input.ICommand AddCompanyCommand
        {
            get
            {
                //@CH 14 oct 2015. Lambda controllen fungerade inte. Flyttade kontrollen till AddComapny metoden.
				_addCompanyCommand = _addCompanyCommand ?? new MvxCommand (AddCompany,() => NewCompanyId != null && NewCompanyId.Length > 0);
                return _addCompanyCommand;
            }
        }
			
        private MvxCommand _loginCommand;
        public ICommand LoginCommand
        {
            get
            {
                _loginCommand = _loginCommand ?? new MvxCommand(Login, () => Password != null && Password.Length > 0 && Username != null && Username.Length > 0);
                return _loginCommand;
            }
        }

        //command implementaions
        public async void AddCompany()
        {
            NewCompanyLoading = true;
            NewCompanyStatus = "Fetching details";
            try
            {
                Company firstCompany = await _companyService.CreateNewCompany(NewCompanyId);
                _companies.Add(firstCompany);

//				Company newCompany = new Company();
//				newCompany.Name = "tse";
//				newCompany.Id = "1234564";
				//_companies.Add(newCompany);
                SelectedCompany = firstCompany;

                Settings.SaveAsJson<Company[]>(_companies.ToArray(), Settings.PathCompanyData, "");
                NewCompanyLoading = false;
                NewCompanyId = "";
                NewCompanyStatus = "";
                ShowAddCompany = false;
            }
            catch (Exception ex)
            {
                NewCompanyStatus = "";
                NewCompanyLoading = false;
                NewCompanyStatus = ex.Message;
            }
        }

	

        public async void Login()
        {
            Debug.WriteLine("LOGIN CALLED");
            IsLoading = true;
            Profile profile = null;       
            try
            {
                if (!_connectionService.IsConnected())
                {

                    _loginService.ComputeHashes(SelectedCompany.Id, Username, Password);

                    string lastUsername = Settings.GetString(Settings.KeyUsername, _loginService.UserPasswordHash);
                    string lastPassword = Settings.GetString(Settings.KeyPassword, _loginService.UserPasswordHash);

                    if (lastUsername != null && lastPassword != null && lastUsername == Username && lastPassword == Password)
                    {
                        profile = Settings.LoadFromJson<Profile>(Settings.PathProfileData, _loginService.CompanyHash, true);
                        profile.News = await ParseRssFeed(profile.RssFeed);
                        _loginService.Profile = profile;
                        TransactionBase.BaseUrl = profile.Url;
                        Theme.AccentColor = profile.AccentColor;
                        IsLoading = false;
                        _loginService.LoggedIn = true;
                        _loginService.StartCountdown();
                        _loginService.InitDb();
                        ShowViewModel<StartViewModel>();

						//@CH  22 october 2014
//						Username = "";
//						Password = "";
                    }
                    else if (lastUsername == null && lastPassword == null)
                    {
                        IsLoading = false;
                        ReportError("You are in offline mode, only previous logged in users can access the service");
                    }
                    else
                    {
                        IsLoading = false;
                        ReportError("Username and/or Password is incorrect");
                    }
                }
                else
                {
                    LoadingText = "Signing in";
                    profile = await _loginService.Login(SelectedCompany.Url, SelectedCompany.Id, Username, Password);
                    //TODO this has been commented out for protection
                    /*if (Username != "******" || Password != "*****")
                    {
                        throw new Exception("Username and/or Password is incorrect");
                    }*/
                    if (_cancelLogin)
                    {
                        return;
                    }
                    //login successfull store username && password
                    Settings.Save(Settings.KeyUsername, Username, _loginService.UserPasswordHash);
                    Settings.Save(Settings.KeyPassword, Password, _loginService.UserPasswordHash);
                    Settings.SaveAsJson<Profile>(profile, Settings.PathProfileData, _loginService.CompanyHash, true);
                    Theme.AccentColor = profile.AccentColor;
                    long lastModifiedProfile =
                        Settings.GetLong(Settings.KeyProfileLastModified + _loginService.CompanyHash);
                    lastModifiedProfile = 0; //TODO remove before demo
                    if (profile.LastModified > lastModifiedProfile)
                    {
                        Settings.Save(Settings.KeyProfileLastModified + _loginService.CompanyHash, profile.LastModified);
                        LoadingText = "Setting up profile";
                        NetworkClient client = new NetworkClient();
						string backgroundPath = _loginService.CompanyHash + _separator.Slash + "background" + Path.GetExtension(profile.BackgroundImage);
						string logoPath = _loginService.CompanyHash + _separator.Slash + "logo" + Path.GetExtension(profile.Logo);
                        try
                        {
                            _filePlugin.WriteFile(backgroundPath, await client.GetByteArray(profile.BackgroundImage));
                        }
                        catch (Exception ex)
                        {
                            var test = 1;
                        }
                        if (_cancelLogin)
                        {
                            return;
                        }
                        try
                        {
                            _filePlugin.WriteFile(logoPath, await client.GetByteArray(profile.Logo));
                        }
                        catch (Exception ex)
                        {
							var test = 1;
                        }
                        if (_cancelLogin)
                        {
                            return;
                        }
                    }
                    LoadingText = "Loading news";
                    profile.News = await ParseRssFeed(profile.RssFeed);
                    if (profile.News == null)
                    {
                        profile.News = new List<FeedItem>();
                    }
                    if (_cancelLogin)
                    {
                        return;
                    }

                    IsLoading = false;
					//@CH  22 october 2014
//					Username = "";
//					Password = "";
                    ShowViewModel<StartViewModel>();

                }
            }
            catch (Exception ex)
            {

                IsLoading = false;
                ReportError("Sorry - "+ ex.Message);
            }
        }

        private async Task<List<FeedItem>> ParseRssFeed(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                if (_connectionService.IsConnected())
                {
                    List<FeedItem> items = new List<FeedItem>();
                    await Task.Run(() =>
                    {
                        FeedParser parser = new FeedParser();
                        items = parser.Parse(url);
                    });
                    Settings.SaveAsJson(items, Settings.PathNewsData, _loginService.CompanyHash, true);
                    return items;
                }
                else
                {
                    try
                    {
                        List<FeedItem> items = Settings.LoadFromJson<List<FeedItem>>(Settings.PathNewsData,
                            _loginService.CompanyHash, true);
                        return items;
                    }
                    catch (Exception ex)
                    {

                    }
                    return new List<FeedItem>();
                }
            }
            else
            {
                return new List<FeedItem>();
            }

        }
    }
}
