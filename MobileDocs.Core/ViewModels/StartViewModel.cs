﻿using System.Diagnostics;
using System.IO;
using Cirrious.MvvmCross.Plugins.WebBrowser;
using Cirrious.MvvmCross.ViewModels;
using HtmlAgilityPack;
using MobileDocs.Core.Common;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;
using MobileDocs.Core.Net;
using MobileDocs.Core.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.CrossCore.Core;

namespace MobileDocs.Core.ViewModels
{
	public class StartViewModel : BaseViewModel
    {
        private Profile profile;
        private readonly ILoginService _loginService;
        private readonly IMvxWebBrowserTask _webBrowserTask;
        private readonly IEncryptionService _encryptionService;
        private readonly IDatabaseService _databaseService;

		private readonly IPlatformFileSystem _platform;


		public class LogoutMessager : MvxMessage
		{
			public LogoutMessager(object sender) : base(sender)
			{
				IsLoggedIn = false;
			}

			public bool IsLoggedIn { get; private set; }
		}







		public StartViewModel(IEncryptionService encryptionService, IMvxWebBrowserTask webBrowserTask, IDatabaseService databaseService, ILoginService loginService, IConnectionService connectionService, IPlatformFileSystem platform){
            _loginService = loginService;
            _webBrowserTask = webBrowserTask;
            _encryptionService = encryptionService;
            _databaseService = databaseService;
			_platform = platform;

            ObservableCollection<ProfileModule> temp = new ObservableCollection<ProfileModule>();

            if (connectionService.IsConnected())
            {
                SyncManager.Resume(loginService);
            }

            
            profile = loginService.Profile;

            bool lastCmis = false;
            foreach (ProfileModule module in profile.Modules)
            {
                if (module.Type == ModuleType.Cmis)
                {
                    lastCmis = true;
                    if (!connectionService.IsConnected())
                    {
                        module.Enabled = false;
                    }
                }
                if (lastCmis && module.Type != ModuleType.Cmis)
                {
                    Modules.Add(new ProfileModule()
                    {
                        Name = "Offline storage",
                        Type = ModuleType.Offline
                    });
                    if (_databaseService.HasFavorites())
                    {
                        Modules.Add(new ProfileModule()
                        {
                            Name = "Favorites",
                            Type = ModuleType.Favorites
                        });
                    }
                }
                Modules.Add(module);
            }
            Modules.Add(new ProfileModule()
            {
                Name = "Logout",
                Type = ModuleType.Logout
            });
				

			BackgroundImageSource = _platform.DataPath + loginService.CompanyHash + "/background" +
				Path.GetExtension(profile.BackgroundImage);

			LogoImageSource = platform.DataPath + loginService.CompanyHash + "/logo" +
				Path.GetExtension(profile.Logo);



//            BackgroundImageSource = "ms-appdata:///local/" + loginService.CompanyHash + "/background" +
//                                    Path.GetExtension(profile.BackgroundImage);
//
//            LogoImageSource = "ms-appdata:///local/" + loginService.CompanyHash + "/logo" +
//                                    Path.GetExtension(profile.Logo);

            RecentItems = new ObservableCollection<FileItem>(databaseService.RecentFiles());

            News = new ObservableCollection<FeedItem>(profile.News);
        }

        


	


        MvxCommand<ProfileModule> _selectModuleCommand;
        public ICommand SelectModuleCommand
        {
            get
            {
                _selectModuleCommand = _selectModuleCommand ?? new MvxCommand<ProfileModule>(SelectModule);
                return _selectModuleCommand;
            }
        }





		public string ProfileAccentColor
		{
			get { return profile.AccentColor;  }
		}

		public string ProfileName
		{
			get { return profile.Name;  }
		}

		public string ProfileLogo
		{
			get { return profile.Logo;  }
		}




        MvxCommand<FeedItem> _selectNewsCommand;
        public ICommand SelectNewsCommand
        {
            get
            {
                _selectNewsCommand = _selectNewsCommand ?? new MvxCommand<FeedItem>(SelectNews);
                return _selectNewsCommand;
            }
        }

        MvxCommand<FileItem> _selectRecentCommand;
        public ICommand SelectRecentCommand
        {
            get
            {
                _selectRecentCommand = _selectRecentCommand ?? new MvxCommand<FileItem>(SelectRecent);
                return _selectRecentCommand;
            }
        }





//		private MvxCommand _;
//		public ICommand LogoutCommand
//		{
//			get
//			{
//				_logoutCommand = _logoutCommand ?? new MvxCommand(Logout);
//				return _logoutCommand;
//			}
//		}


        private MvxCommand _logoutCommand;
        public ICommand LogoutCommand
        {
            get
            {
                _logoutCommand = _logoutCommand ?? new MvxCommand(Logout);
                return _logoutCommand;
            }
        }


		private MvxCommand _LogoutCommandWithMessageOnlyThatShouldBeRefactored;
		public ICommand LogoutCommandWithMessageOnlyThatShouldBeRefactored
		{
			get
			{
				_LogoutCommandWithMessageOnlyThatShouldBeRefactored = _LogoutCommandWithMessageOnlyThatShouldBeRefactored ?? new MvxCommand(Logout2);
				return _LogoutCommandWithMessageOnlyThatShouldBeRefactored;
			}
		}

        public void Logout()
        {
            _loginService.Logout();
            ShowViewModelAndClearBackStack<LoginViewModel>();

			//var _messenger = Mvx.Resolve<IMvxMessenger>();
			//_messenger.Publish(new LogoutMessager(this));
        }

		public void Logout2()
		{
			_loginService.Logout();
		
			var _messenger = Mvx.Resolve<IMvxMessenger>();
			_messenger.Publish(new LogoutMessager(this));
		}

        private void SelectNews(FeedItem feedItem)
        {
            _webBrowserTask.ShowWebPage(feedItem.Link);
        }

        private void SelectRecent(FileItem fileItem)
        {
            DocumentParameters docParams = new DocumentParameters() { Name = fileItem.Name, Id = fileItem.Id };
            string fileHash = _encryptionService.Sha256(fileItem.Id);
	

			string localFolder = _loginService.CompanyHash + _platform.Slash + "files" + _platform.Slash;
            string fullPath = localFolder + fileHash;
            docParams.Path = fullPath;
            docParams.IsSynchronized = true;
            docParams.IsFavorite = _databaseService.IsFavorite(fileItem.Id);
            ShowViewModel<DocumentViewModel>(docParams);
        }



		/*
		 * IOS HACK @CL_10nov14 - Data Binding did't work so I had to do this.
		 */
		public void selectModuleIOS(ProfileModule p) {

			SelectModule (p);
		}

        private void SelectModule(ProfileModule p)
        {
            if (p.Enabled) { 
                switch (p.Type)
                {
                    case ModuleType.Logout:
                        Logout();
                        break;
                    case ModuleType.Favorites:
                        ShowViewModel<FavoritesViewModel>();
                        break;
                    case ModuleType.Cmis:

				
				
                        ShowViewModel<BrowseViewModel>(new BrowseParameters(){ RepoUrl = p.UrlBrowser, Type = BrowseType.Remote, RepoName = p.RepoName});
                        break;
                    case ModuleType.Offline:
                        ShowViewModel<BrowseViewModel>(new BrowseParameters() { Type = BrowseType.Local });
                        break;
                    case ModuleType.Link:
                        _webBrowserTask.ShowWebPage(p.Url);
                        break;
                }
            }
        }


        private string _backgroundImageSource;
        public string BackgroundImageSource
        {
            get { return _backgroundImageSource; }
            set { _backgroundImageSource = value; RaisePropertyChanged(() => BackgroundImageSource); }
        }


        private string _logoImageSource;
        public string LogoImageSource
        {
            get { return _logoImageSource; }
            set { _logoImageSource = value; RaisePropertyChanged(() => LogoImageSource); }
        }
        

        



        private ObservableCollection<ProfileModule> _modules = new ObservableCollection<ProfileModule>();
        public ObservableCollection<ProfileModule> Modules
        {
            get { return _modules; }
            set { _modules = value; RaisePropertyChanged(() => Modules); }
        }

        private ObservableCollection<FeedItem> _news;
        public ObservableCollection<FeedItem> News
        {
            get { return _news; }
            set { _news = value; RaisePropertyChanged(() => News); }
        }

        private ObservableCollection<FileItem> _recentItems;
        public ObservableCollection<FileItem> RecentItems
        {
            get { return _recentItems; }
            set { _recentItems = value; RaisePropertyChanged(() => RecentItems); }
        }
        
    }
}
