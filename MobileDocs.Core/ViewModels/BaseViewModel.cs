﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//    Defines the BaseViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using MobileDocs.Core.Common;

namespace MobileDocs.Core.ViewModels
{
    using System;
    using System.Linq.Expressions;
    using Cirrious.CrossCore;
    using Cirrious.MvvmCross.ViewModels;
    using MobileDocs.Core.Interfaces;
    using MobileDocs.Core.Net;
    using System.Threading.Tasks;

    /// <summary>
    ///    Defines the BaseViewModel type.
    /// </summary>
    public class BaseViewModel : MvxViewModel
    {

        protected void ReportError(string text)
        {
            Mvx.Resolve<IErrorReporter>().ReportError(text);
        }

        protected void ShowViewModelAndClearBackStack<T>()
            where T : BaseViewModel
        {
            //ShowViewModel<T>();
			ChangePresentation(new ClearNavBackStackHint());
        }

        /*public async Task<T> GeneralAsyncLoad<T>(TransactionBase transaction)
        {
            try
            {
                IsLoading = true;
                var t = typeof(T);
                if(t != transaction.GetType()){
                    throw new ArgumentException(typeof(T).FullName + "Must be a  transaction.GetType() = " + transaction.GetType());
                }
                return await transaction.ExecuteAsync<T>();
            }
            catch (Exception exception)
            {
                IsLoading = false;
                ReportError("Sorry - problem seen " + exception.Message);
                return default(T);
            }
        }*/

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { _isLoading = value; RaisePropertyChanged(() => IsLoading); }
        }


        private string _loadingMessage;
        public string LoadingMessage
        {
            get { return _loadingMessage; }
            set { _loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }
        

    }
}
