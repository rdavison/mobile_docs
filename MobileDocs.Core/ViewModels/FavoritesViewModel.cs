﻿using Cirrious.MvvmCross.ViewModels;
using MobileDocs.Core.Interfaces;
using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MobileDocs.Core.ViewModels
{
    public class FavoritesViewModel : BaseViewModel
    {

        private readonly ILoginService _loginService;
        private readonly IEncryptionService _encryptionService;

        public FavoritesViewModel(ILoginService loginService, IEncryptionService encryptionService, IDatabaseService databaseService)
        {
            _loginService = loginService;
            _encryptionService = encryptionService;

            Items = new ObservableCollection<FileItem>(databaseService.Favorites());
        }

        MvxCommand<FileItem> _selectFavoriteCommand;
        public ICommand SelectFavoriteCommand
        {
            get
            {
                _selectFavoriteCommand = _selectFavoriteCommand ?? new MvxCommand<FileItem>(SelectFavorite);
                return _selectFavoriteCommand;
            }
        }

        private void SelectFavorite(FileItem fileItem)
        {

            DocumentParameters docParams = new DocumentParameters() { Name = fileItem.Name, Id = fileItem.Id };
            string fileHash = _encryptionService.Sha256(fileItem.Id);
            string localFolder = _loginService.CompanyHash + "\\files\\";
            string fullPath = localFolder + fileHash;
            docParams.Path = fullPath;
            docParams.IsSynchronized = true;
            docParams.IsFavorite = true;
            ShowViewModel<DocumentViewModel>(docParams);
        }

        private ObservableCollection<FileItem> _items = new ObservableCollection<FileItem>();
        public ObservableCollection<FileItem> Items
        {
            get { return _items; }
            set { _items = value; RaisePropertyChanged(() => Items); }
        }

        private MvxCommand _goBackCommand;
        public ICommand GoBackCommand
        {
            get
            {
                _goBackCommand = _goBackCommand ?? new MvxCommand(() => Close(this));
                return _goBackCommand;
            }
        }




    }
}
