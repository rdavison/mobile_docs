using System.Runtime.InteropServices;
using MobileDocs.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Storage.Streams;
using Windows.Security.Cryptography.Core;
using System.Runtime.InteropServices.WindowsRuntime;

namespace MobileDocs.WindowsStore.Common
{
    public class StoreEncryptionService : IEncryptionService
    {

        private const string ConstSalt = "pM25_JkCrCNK&tJz|V";
        private const string Password = "jJ_xNt^)(p/mFsEMNaL#24nlj`a8DHDp-jnR+oRgY#9Sl<p/Vyq%1P3:@5#WDlJm";
        private const uint IterationCount = 10000;



        

        private static void GenerateKeyMaterial(string password, string salt, uint iterationCount, out IBuffer keyMaterial, out IBuffer iv)
        {
            // Setup KDF parameters for the desired salt and iteration count
            IBuffer saltBuffer = CryptographicBuffer.ConvertStringToBinary(salt, BinaryStringEncoding.Utf8);
            KeyDerivationParameters kdfParameters = KeyDerivationParameters.BuildForPbkdf2(saltBuffer, iterationCount);
 
            // Get a KDF provider for PBKDF2, and store the source password in a Cryptographic Key
            KeyDerivationAlgorithmProvider kdf = KeyDerivationAlgorithmProvider.OpenAlgorithm(KeyDerivationAlgorithmNames.Pbkdf2Sha1);
            IBuffer passwordBuffer = CryptographicBuffer.ConvertStringToBinary(password, BinaryStringEncoding.Utf8);
            CryptographicKey passwordSourceKey = kdf.CreateKey(passwordBuffer);
 
            // Generate key material from the source password, salt, and iteration count.  Only call DeriveKeyMaterial once,
            // since calling it twice will generate the same data for the key and IV.
            int keySize = 256 / 8;
            int ivSize = 128 / 8;
            uint totalDataNeeded = (uint)(keySize + ivSize);
            IBuffer keyAndIv = CryptographicEngine.DeriveKeyMaterial(passwordSourceKey, kdfParameters, totalDataNeeded);
 
            // Split the derived bytes into a seperate key and IV
            byte[] keyMaterialBytes = keyAndIv.ToArray();
            keyMaterial = WindowsRuntimeBuffer.Create(keyMaterialBytes, 0, keySize, keySize);
            iv = WindowsRuntimeBuffer.Create(keyMaterialBytes, keySize, ivSize, ivSize);
        }

        //public byte[] Encrypt(byte[] toEncrypt, string salt)
        //{
        //    IBuffer aesKeyMaterial;
        //    IBuffer iv;

        //    GenerateKeyMaterial(Password, ConstSalt + salt, IterationCount, out aesKeyMaterial, out iv);

        //    IBuffer plainText = CryptographicBuffer.CreateFromByteArray(toEncrypt);

        //    // Setup an AES key, using AES in CBC mode and applying PKCS#7 padding on the input
        //    SymmetricKeyAlgorithmProvider aesProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
        //    CryptographicKey aesKey = aesProvider.CreateSymmetricKey(aesKeyMaterial);

        //    // Encrypt the data and convert it to a Base64 string
        //    IBuffer encrypted = CryptographicEngine.Encrypt(aesKey, plainText, iv);

        //    byte[] byteArray;
        //    CryptographicBuffer.CopyToByteArray(encrypted, out byteArray);
        //    return byteArray;
        //}

        //public byte[] Decrypt(byte[] cipher, string salt)
        //{
        //    IBuffer aesKeyMaterial;
        //    IBuffer iv;
        //    GenerateKeyMaterial(Password, ConstSalt + salt, IterationCount, out aesKeyMaterial, out iv);

        //    // Setup an AES key, using AES in CBC mode and applying PKCS#7 padding on the input
        //    SymmetricKeyAlgorithmProvider aesProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
        //    CryptographicKey aesKey = aesProvider.CreateSymmetricKey(aesKeyMaterial);

        //    // Convert the base64 input to an IBuffer for decryption
        //    IBuffer ciphertext = CryptographicBuffer.CreateFromByteArray(cipher);

        //    // Decrypt the data and convert it back to a string
        //    IBuffer decrypted = CryptographicEngine.Decrypt(aesKey, ciphertext, iv);
        //    byte[] decryptedArray = decrypted.ToArray();
        //    return decryptedArray; //Encoding.UTF8.GetString(decryptedArray, 0, decryptedArray.Length);
        //}

        public byte[] Encrypt(byte[] data, string salt)
        {
            CryptographicKey symmKey;
            IBuffer buffer;
            IBuffer saltMaterial;
            Setup(data, salt, out symmKey, out buffer, out saltMaterial);
            IBuffer resultBuffer = CryptographicEngine.Encrypt(symmKey, buffer, saltMaterial);
            byte[] result;
            CryptographicBuffer.CopyToByteArray(resultBuffer, out result);

            return result;
        }

        public byte[] Decrypt(byte[] encryptedData, string salt)
        {
            CryptographicKey symmKey;
            IBuffer buffer;
            IBuffer saltMaterial;
            Setup(encryptedData, salt, out symmKey, out buffer, out saltMaterial);
            IBuffer resultBuffer = CryptographicEngine.Decrypt(symmKey, buffer, saltMaterial);
            byte[] decryptedBytes;
            CryptographicBuffer.CopyToByteArray(resultBuffer, out decryptedBytes);
            return decryptedBytes;
        }

        private void Setup(byte[] data, string salt, out CryptographicKey symmKey, out IBuffer buffer,
            out IBuffer saltMaterial)
        {
            IBuffer pwBuffer = CryptographicBuffer.ConvertStringToBinary(Password, BinaryStringEncoding.Utf8);
            IBuffer saltBuffer = CryptographicBuffer.ConvertStringToBinary(ConstSalt + salt, BinaryStringEncoding.Utf8);
            buffer = CryptographicBuffer.CreateFromByteArray(data);

            // Derive key material for password size 32 bytes for AES256 algorithm
            KeyDerivationAlgorithmProvider keyDerivationProvider = KeyDerivationAlgorithmProvider.OpenAlgorithm(KeyDerivationAlgorithmNames.Pbkdf2Sha1);
            // using salt and 1000 iterations
            KeyDerivationParameters pbkdf2Parms = KeyDerivationParameters.BuildForPbkdf2(saltBuffer, 1000);

            // create a key based on original key and derivation parmaters
            CryptographicKey keyOriginal = keyDerivationProvider.CreateKey(pwBuffer);
            IBuffer keyMaterial = CryptographicEngine.DeriveKeyMaterial(keyOriginal, pbkdf2Parms, 32);
            CryptographicKey derivedPwKey = keyDerivationProvider.CreateKey(pwBuffer);

            // derive buffer to be used for encryption salt from derived password key 
            saltMaterial = CryptographicEngine.DeriveKeyMaterial(derivedPwKey, pbkdf2Parms, 16);

            // display the keys – because KeyDerivationProvider always gets cleared after each use, they are very similar unforunately
            string keyMaterialString = CryptographicBuffer.EncodeToBase64String(keyMaterial);
            string saltMaterialString = CryptographicBuffer.EncodeToBase64String(saltMaterial);

            SymmetricKeyAlgorithmProvider symProvider =
                SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
            // create symmetric key from derived password material
            symmKey = symProvider.CreateSymmetricKey(keyMaterial);
        }

        public string Encrypt(string toEncrypt, string salt)
        {
            byte[] encryptedBytes = Encrypt(Encoding.UTF8.GetBytes(toEncrypt), salt);
            return Encoding.UTF8.GetString(encryptedBytes, 0, encryptedBytes.Length);
        }

        public string Decrypt(string cipher, string salt)
        {
            byte[] decryptedBytes = Decrypt(Encoding.UTF8.GetBytes(cipher), salt);
            return Encoding.UTF8.GetString(decryptedBytes, 0, decryptedBytes.Length);
        }


    
        //public string Encrypt(string toEncrypt, string salt)
        //{
        //    IBuffer aesKeyMaterial;
        //    IBuffer iv;

        //    GenerateKeyMaterial(Password, ConstSalt + salt, IterationCount, out aesKeyMaterial, out iv);
 
        //    IBuffer plainText = CryptographicBuffer.ConvertStringToBinary(toEncrypt, BinaryStringEncoding.Utf8);
 
        //    // Setup an AES key, using AES in CBC mode and applying PKCS#7 padding on the input
        //    SymmetricKeyAlgorithmProvider aesProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
        //    CryptographicKey aesKey = aesProvider.CreateSymmetricKey(aesKeyMaterial);
 
        //    // Encrypt the data and convert it to a Base64 string
        //    IBuffer encrypted = CryptographicEngine.Encrypt(aesKey, plainText, iv);
        //    return CryptographicBuffer.EncodeToBase64String(encrypted);
        //}

        //public string Decrypt(string cipherString, string salt)
        //{
        //    IBuffer aesKeyMaterial;
        //    IBuffer iv;
        //    GenerateKeyMaterial(Password, ConstSalt+salt, IterationCount, out aesKeyMaterial, out iv);

        //    // Setup an AES key, using AES in CBC mode and applying PKCS#7 padding on the input
        //    SymmetricKeyAlgorithmProvider aesProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
        //    CryptographicKey aesKey = aesProvider.CreateSymmetricKey(aesKeyMaterial);

        //    // Convert the base64 input to an IBuffer for decryption
        //    IBuffer ciphertext = CryptographicBuffer.DecodeFromBase64String(cipherString);

        //    // Decrypt the data and convert it back to a string
        //    IBuffer decrypted = CryptographicEngine.Decrypt(aesKey, ciphertext, iv);
        //    byte[] decryptedArray = decrypted.ToArray();
        //    return Encoding.UTF8.GetString(decryptedArray, 0, decryptedArray.Length);
        //}


        public string SHA256(string toHash)
        {
            IBuffer input = CryptographicBuffer.ConvertStringToBinary(toHash,
            BinaryStringEncoding.Utf8);
            var hasher = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256);
            IBuffer hashed = hasher.HashData(input);
            return CryptographicBuffer.EncodeToHexString(hashed);
        }

        
    }
}
