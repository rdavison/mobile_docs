using System.Runtime.InteropServices;
using Cirrious.CrossCore;
using MobileDocs.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCLCrypto;

//using Windows.Security.Cryptography;
//using Windows.Storage.Streams;
//using Windows.Security.Cryptography.Core;
using System.Runtime.InteropServices.WindowsRuntime;

namespace MobileDocs.Ios.Services
{
    public class IosEncryptionService : IEncryptionService
    {

        private const string ConstSalt = "WAkPwyaT,X#)n{DZAq&|Vc0&ynOj (f(t0C1#Nh-G5)X?%l.<i*Zf%j)K/uR.crI";
        private const string Password = "G?14?EPak}~pg)C~wd=-4$a(gFp1<fI>Laxve]MESW~!z|b|8Rfl;Yj7`y/k{fWl";
        private const uint IterationCount = 10000;

        private static void PadToMultipleOf(ref byte[] src, int pad)
        {
            int len = (src.Length + pad - 1) / pad * pad;
            Array.Resize(ref src, len);
        }

        
        public byte[] Encrypt(byte[] toEncrypt, string salt)
        {

            return toEncrypt;
        }

        public byte[] Decrypt(byte[] cipher, string salt)
        {

            return cipher;
        }



        public string Encrypt(string toEncrypt, string salt)
        {
            byte[] encryptedBytes = Encrypt(Encoding.UTF8.GetBytes(toEncrypt), salt);
            return Convert.ToBase64String(encryptedBytes);
            //return Encoding.UTF8.GetString(encryptedBytes, 0, encryptedBytes.Length);
        }

        public string Decrypt(string cipher, string salt)
        {
            byte[] decryptedBytes = Decrypt(Convert.FromBase64String(cipher), salt);
            //remove empty bytes caused by encryption padding
            var i = decryptedBytes.Length - 1;
            while (decryptedBytes[i] == 0)
            {
                --i;
            }
            return Encoding.UTF8.GetString(decryptedBytes, 0, i + 1);
        }


        private byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        public string Sha256(string toHash)
        {

            byte[] data = GetBytes(toHash);
            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(PCLCrypto.HashAlgorithm.Sha256);
            byte[] hash = hasher.HashData(data);
            string hashBase64 = Convert.ToBase64String(hash);

            return hashBase64;
         
        }


      

    }
}
