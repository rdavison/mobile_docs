using MonoTouch.UIKit;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Touch.Platform;
using Cirrious.CrossCore;
using MobileDocs.Core.Interfaces;
using MobileDocs.Ios.Services;
using System.Collections.Generic;
using System.Reflection;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using MobileDocs.Ios.Common;
using MobileDocs.Core;

namespace MobileDocs.Ios
{
	public class Setup : MvxTouchSetup
	{

		private UIWindow _window;
		private MvxApplicationDelegate _applicationDelegate;
		public Setup(MvxApplicationDelegate applicationDelegate, UIWindow window)
            : base(applicationDelegate, window)
		{
			_window = window;

		}

//
//		protected override List<Assembly> ValueConverterAssemblies
//		{
//			get
//			{
//				var toReturn = base.ValueConverterAssemblies;
//				toReturn.Add(typeof (MvxNativeColorValueConverter).Assembly);
//				toReturn.Add(typeof (MvxVisibilityValueConverter).Assembly);
//				return toReturn;
//			}
//		}

		protected override IMvxTouchViewPresenter CreatePresenter ()
		{
			var presenter = new IosViewPresenter (_applicationDelegate, _window);
			//Mvx.RegisterSingleton (presenter);
			return presenter;
		}

		protected override IMvxApplication CreateApp ()
		{
			return new Core.App();
		}
		
        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();
            Mvx.RegisterSingleton<IEncryptionService>(new IosEncryptionService());
            Mvx.RegisterSingleton<IConnectionService>(new IosConnectionService());
			Mvx.RegisterSingleton<IPlatformFileSystem>(new IOSFileSystem());

        }
	}
}