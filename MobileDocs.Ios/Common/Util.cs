﻿using System;
using MonoTouch.UIKit;

namespace MobileDocs.Ios.Common
{
	public class Util
	{

		public static bool isLandScape() {

			UIDeviceOrientation deviceOrientation = UIDevice.CurrentDevice.Orientation;

			if((deviceOrientation == UIDeviceOrientation.LandscapeLeft || deviceOrientation == UIDeviceOrientation.LandscapeRight) && deviceOrientation != UIDeviceOrientation.Unknown) {
				return true;
			}
			return false;

		}


		public static bool isPortrait() {

			UIDeviceOrientation deviceOrientation = UIDevice.CurrentDevice.Orientation;

			if((deviceOrientation == UIDeviceOrientation.Portrait || deviceOrientation == UIDeviceOrientation.PortraitUpsideDown) && deviceOrientation != UIDeviceOrientation.Unknown) {
				return true;
			}
			return false;

		}
	}
}

