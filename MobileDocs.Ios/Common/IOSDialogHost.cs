
using MobileDocs.Core.ViewModels;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Collections.Generic;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MobileDocs.Ios.Extension;
using System;
using Cirrious.MvvmCross.ViewModels;
using System.Drawing;
using MobileDocs.Ios.Common;
using Cirrious.MvvmCross.Binding.Touch.Views;

namespace MobileDocs.Ios.Common
{



	public interface IOSDialogHost<T,R> where T : MvxViewController where R : MvxViewModel 
	{
	
		R GetViewModel();
		MvxFluentBindingDescriptionSet<T, R> GetBindingDescriptionSet();
	}




}