﻿using System;
using MobileDocs.Core;

namespace MobileDocs.Ios
{
	public class IOSFileSystem : IPlatformFileSystem
	{
		public  string Slash {
			get {
				return "/";
			}
		}


		public  string DataPath {
			get {
				return Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/";
			}
		}



	}
}

