﻿using System;

namespace MobileDocs.Ios.Common
{

	public class ViewWillDisappearEventArgs : System.EventArgs
	{
		public string SelectedProfile { get; set; }
	}
	public class ViewHandler {
		public event EventHandler ViewLoadEvent;
		public event EventHandler ViewShowingEvent;
		public event EventHandler ViewHiddenEvent;

		public enum Event { ViewDidLoad, ViewShowing, ViewWillDisappear};


		public void Fire(Event evt) {
			Fire (evt, null);
		}
		public void Fire(Event evt, Object obj) {

			switch (evt) {

			case Event.ViewWillDisappear:

				if (ViewHiddenEvent != null) {
					ViewHiddenEvent(this, (ViewWillDisappearEventArgs) obj);
				}
				break;

			case Event.ViewShowing:

				if (ViewShowingEvent != null) {
					ViewShowingEvent(this, EventArgs.Empty);
				}
				break;


			case Event.ViewDidLoad:

				if (ViewLoadEvent != null) {
					ViewLoadEvent(this, EventArgs.Empty);
				}

				break;

			}

		}

	}
}




