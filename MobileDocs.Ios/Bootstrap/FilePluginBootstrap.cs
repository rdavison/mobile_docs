using Cirrious.CrossCore.Plugins;

namespace MobileDocs.Ios.Bootstrap
{
    public class FilePluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.File.PluginLoader, Cirrious.MvvmCross.Plugins.File.Touch.Plugin>
    {
    }
}