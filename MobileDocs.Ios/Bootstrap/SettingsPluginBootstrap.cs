// Bootstrap/SettingsPluginBootstrap.cs
using Cirrious.CrossCore.Plugins;

 namespace MobileDocs.Ios.Bootstrap
 {
    public class SettingsPluginBootstrap
    	: MvxLoaderPluginBootstrapAction<Refractored.MvxPlugins.Settings.PluginLoader, Refractored.MvxPlugins.Settings.Touch.Plugin>
    {
    }
 }