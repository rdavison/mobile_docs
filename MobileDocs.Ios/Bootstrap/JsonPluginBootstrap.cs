using Cirrious.CrossCore.Plugins;

namespace MobileDocs.Ios.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}