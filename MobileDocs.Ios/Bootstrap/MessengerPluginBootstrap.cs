using Cirrious.CrossCore.Plugins;

namespace MobileDocs.Ios.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}