using Cirrious.CrossCore.Plugins;

namespace MobileDocs.Ios.Bootstrap
{
    public class WebBrowserPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.WebBrowser.PluginLoader, Cirrious.MvvmCross.Plugins.WebBrowser.Touch.Plugin>
    {
    }
}