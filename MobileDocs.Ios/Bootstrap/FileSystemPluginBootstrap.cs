using Cirrious.CrossCore.Plugins;

namespace MobileDocs.Ios.Bootstrap
{
    public class FileSystemPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Acr.MvvmCross.Plugins.FileSystem.PluginLoader, Acr.MvvmCross.Plugins.FileSystem.Touch.Plugin>
    {
    }
}