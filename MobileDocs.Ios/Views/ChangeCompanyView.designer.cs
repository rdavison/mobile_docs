// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace MobileDocs.Ios
{
	[Register ("ChangeCompanyView")]
	partial class ChangeCompanyView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnCancel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnOk { get; set; }

		[Outlet]
		MonoTouch.UIKit.UINavigationBar navTop { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView tblProfiles { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnCancel != null) {
				btnCancel.Dispose ();
				btnCancel = null;
			}

			if (btnOk != null) {
				btnOk.Dispose ();
				btnOk = null;
			}

			if (tblProfiles != null) {
				tblProfiles.Dispose ();
				tblProfiles = null;
			}

			if (navTop != null) {
				navTop.Dispose ();
				navTop = null;
			}
		}
	}
}
