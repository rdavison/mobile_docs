﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using MobileDocs.Core.ViewModels;
using MobileDocs.Core.Models;

namespace MobileDocs.Ios
{
	public partial class ModuleCollectionViewCell : MvxCollectionViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("ModuleCollectionViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ModuleCollectionViewCell");

	//	private readonly MvxImageViewLoader _loader;

		public ModuleCollectionViewCell (IntPtr handle) : base (handle)
		{
			//_loader = new MvxImageViewLoader (() => imgLogo);




//			var StartViewModel =((StartViewModel)base.DataContext);
//
//
//			var test = StartViewModel.LogoImageSource;
		

			this.DelayBind (() => {

				var set = this.CreateBindingSet<ModuleCollectionViewCell,ProfileModule>();

				ProfileModule profileModule = ((ProfileModule)this.DataContext);

				var icon = profileModule.Icon;

				//this.imgLogo.Image = UIImage.FromResource(null,icon);
				set.Bind(imgLogo).To(p => p.Icon).Apply();
				set.Bind(lblText).To(p => p.Name).Apply();


				//var set2 = this.CreateBindingSet<ModuleCollectionViewCell,StartViewModel>();

				//set2.Bind(btnTest).To(p => p.SelectModuleCommand);





				//set.Bind(this.btnTest).To (vm => vm).Apply();

				set.Apply();

			});

	
				
		}

	
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
		}

//		public override void ApplyLayoutAttributes (UICollectionViewLayoutAttributes layoutAttributes)
//		{
//
//			layoutAttributes.Size = new SizeF (500f, 500f);
//			base.ApplyLayoutAttributes (layoutAttributes);
//		}
	public static ModuleCollectionViewCell Create ()
		{
			return (ModuleCollectionViewCell)Nib.Instantiate (null, null) [0];
		}
	}
}

