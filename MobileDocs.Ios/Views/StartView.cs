﻿
//
// TODO Change Tint color on statusbar (dark or white) depending on the background image
//

using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using MobileDocs.Core.ViewModels;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.CrossCore;

using Cirrious.MvvmCross.Plugins.Messenger;

using MobileDocs.Ios.Common;
using MobileDocs.Ios.Extension;
using Cirrious.MvvmCross.Binding.Touch.Views;
using MobileDocs.Core.Models;
using System.Collections.Generic;

namespace MobileDocs.Ios
{
	public partial class StartView : MvxViewController
	{

		public StartView () : base ("StartView", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}


		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			base.NavigationController.NavigationBarHidden = true;
			positionButtons ();
		}





		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();



			base.NavigationController.NavigationBarHidden = true;
			
			
		//
		// Init Stuff
		//

			var StartViewModel = ((StartViewModel)this.DataContext);



		//
		// Create Navbar
		//

			UIBarButtonItem btnLogout;
		
			this.NavigationItem.SetLeftBarButtonItem(
				(btnLogout = new UIBarButtonItem ("Logout", UIBarButtonItemStyle.Done, null))
			, true);


		//
		// Create Bindingset
		//
			var set = this.CreateBindingSet<StartView, StartViewModel> ();
		
			set.Bind (btnLogout).To (m => m.LogoutCommand).Apply();

			UIImage uiImage = UIImage.FromFile(StartViewModel.BackgroundImageSource);
			imgBackground.Image = uiImage;

			viewAccentColor.BackgroundColor = UIColor.Clear.FromHexString (StartViewModel.ProfileAccentColor);

			imgBottomRightLogo.Image = UIImage.FromFile(StartViewModel.LogoImageSource);
			lblCompanyLogo.Text = StartViewModel.ProfileName;



		
		//
		// Create Modules
		//


			//createModuleButtons ();
//
//			var collectionView = new ModuleCollectionView ();
//
//			this.Add(collectionView.CollectionView);
//
//
//
//

			createModuleButtons();


			//set.Bind (source).For(s => s).To (vm => vm.SelectModuleCommand).Apply();


	

//			var collectionView = new ModuleCollectionView ();
//
//
//			collectionView.View.Bounds = new RectangleF (new PointF (0f, 0f), new SizeF (200f, 200f));
//			Add(collectionView.CollectionView);
//

			//collectionView.SizeToFit ();






		//
		// Listen to logout success event and close the view
		//
		//	var _messenger = Mvx.Resolve<IMvxMessenger>();
//			_logoutMessageToken = _messenger.SubscribeOnMainThread<StartViewModel.LogoutMessager>((s) => {
//
//				_messenger.Unsubscribe<StartViewModel.LogoutMessager>(_logoutMessageToken);
//				NavigationController.PopViewControllerAnimated(true);
//
//				NavigationController.
//
//			},MvxReference.Strong);

		}


		public override void WillRotate (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate (toInterfaceOrientation, duration);
			positionButtons(UIScreen.MainScreen.Bounds.Height, UIScreen.MainScreen.Bounds.Width);
		}


		private int spaceBetweenModuleButtons = 10;
		private int moduleButtonsIndex = 0;
		private List<UIView> moduleButtonsWrapperViews = new List<UIView>();
		private List<UILabel> moduleLabels = new List<UILabel>();
		private List<UIImageView> moduleImages = new List<UIImageView>();
		private List<UIButton> moduleButtons = new List<UIButton>();


		/// <summary>
		/// Set position of modules buttons depending on device orientation.
		/// </summary>
		private void positionButtons(float width = 0, float height = 0) {


			if (width == 0) {
				width = UIScreen.MainScreen.Bounds.Width;
			}

			if (height == 0) {
				height = UIScreen.MainScreen.Bounds.Height;
			}

			var spacePerItem = width / moduleButtonsWrapperViews.Count;
		
			foreach (UIView item in moduleButtonsWrapperViews) {

				var index = moduleButtonsWrapperViews.IndexOf(item);
				var wrapperWidth = (spacePerItem * index);

				// Adjust View Size
				item.Frame = new RectangleF(wrapperWidth, height/2 + 20, spacePerItem, 120);


				// Adjust Label Position
				var label = moduleLabels[index];
				label.Frame = new RectangleF(0,90, spacePerItem, 30);

		
				// Adjust Image Position
				var image = moduleImages[index];
				var leftPos = spacePerItem/2 - image.Frame.Width / 2;
				image.Frame = new RectangleF(leftPos, 20, image.Frame.Width, image.Frame.Height);

				// Adjust Button Position
				var button = moduleButtons[index];
				button.Frame = new RectangleF(0, 0, spacePerItem, 120);
		

			}

		}

		/// <summary>
		/// Create Modules buttons
		/// </summary>
		private void createModuleButtons ()
		{

			var StartViewModel = ((StartViewModel)this.DataContext);

			foreach(ProfileModule item in StartViewModel.Modules) {

				if (!item.Enabled)
					continue;
					
				//
				// Create Button Wrapper
				//
				var buttonWrapperView = new UIView();

				//
				// Create Label
				//
				var label = new UILabel(new RectangleF(0,0, 100, 30));
				label.Text = item.Name;
				label.TextColor = UIColor.Clear.FromHexString(StartViewModel.ProfileAccentColor);
				label.TextAlignment = UITextAlignment.Center;
				moduleLabels.Add(label);

				//
				// Create Image
				//
				//TODO Add an RegisterSingleton in setup.cs that acts like IOSFileSystem.cs
				var imageView = new UIImageView (new RectangleF(0,0, 64, 64));
				var image = UIImage.FromFile(item.Icon.Replace(".png", "@2x.png"));
				image = image.ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate);

				imageView.Image = image;
				imageView.TintColor = UIColor.Clear.FromHexString(StartViewModel.ProfileAccentColor);
				moduleImages.Add (imageView);

				//
				// Create Button
				//
				var moduleButton = new UIButton(new RectangleF(0,0, 64, 64));

				moduleButtons.Add (moduleButton);


				// TODO Change Tint mask to dark when holding on the button and revert
				// on TouchUpInside or TouchCancel ecent
				moduleButton.TouchUpInside += (object sender, EventArgs e) => {

					if(item.Type == ModuleType.Cmis || item.Type == ModuleType.Offline) {

						// Load viewmodels manually and create custom MvxTouchViewsContainer with
						// Hamburger menu and document view
						this.NavigationController.PushViewController(new SideBar(item), true);
					}
					else {
						StartViewModel.selectModuleIOS(item);
					}
				};



				//
				// Add Module contents
				//
				buttonWrapperView.Add(label);
				buttonWrapperView.Add(imageView);
				buttonWrapperView.Add(moduleButton);

				//
				// Add module to wrapper
				//
				moduleButtonsWrapperViews.Add (buttonWrapperView);
				View.Add (buttonWrapperView);
				moduleButtonsIndex++;

			}




		}
	}
}

