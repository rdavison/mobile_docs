// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace MobileDocs.Ios
{
	[Register ("ChoiceCompany")]
	partial class ChoiceCompany
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnCancel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnOk { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView indLoading { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblLoading { get; set; }

		[Outlet]
		MonoTouch.UIKit.UINavigationBar navBar { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtKey { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnCancel != null) {
				btnCancel.Dispose ();
				btnCancel = null;
			}

			if (btnOk != null) {
				btnOk.Dispose ();
				btnOk = null;
			}

			if (indLoading != null) {
				indLoading.Dispose ();
				indLoading = null;
			}

			if (lblLoading != null) {
				lblLoading.Dispose ();
				lblLoading = null;
			}

			if (navBar != null) {
				navBar.Dispose ();
				navBar = null;
			}

			if (txtKey != null) {
				txtKey.Dispose ();
				txtKey = null;
			}
		}
	}
}
