﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using MobileDocs.Ios.Common;
using MobileDocs.Core.ViewModels;
using Cirrious.MvvmCross.Touch.Views;
using MobileDocs.Ios.Extension;

namespace MobileDocs.Ios
{



	public partial class ChoiceCompany : UIViewController
	{

		public UIButton BtnCancel;
		public UIButton BtnOk;
		public UITextField TxtKey;
		public UIActivityIndicatorView IndLoading;
		public UILabel LblLoading;

		public ViewHandler ViewHandler = new ViewHandler();

		public ChoiceCompany () : base ("ChoiceCompany", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public MvxViewController Parent {
			get;
			set;
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			var iosDialogHost = (Parent as IOSDialogHost<LoginView, LoginViewModel>);
			var set = iosDialogHost.GetBindingDescriptionSet();

			this.btnOk.SetTitleColor(UIColor.Clear.FromHex(0xd3d3d3), UIControlState.Disabled);
			set.Bind(this.btnOk).To((LoginViewModel vm) => vm.AddCompanyCommand).Apply();
			set.Bind (this.btnOk).For (btnOk => btnOk.Enabled).To ((LoginViewModel vm) => vm.NewCompanyLoading).WithConversion("BooleanInverted").Apply();




			set.Bind(this.btnCancel).To((LoginViewModel vm) => vm.HideAddCompanyCommand).Apply();
			set.Bind(this.txtKey).To((LoginViewModel vm) => vm.NewCompanyId).Apply();
			set.Bind(this.lblLoading).To((LoginViewModel vm) => vm.NewCompanyStatus).Apply();
			set.Bind(this.indLoading).For(spin => spin.Hidden).To((LoginViewModel vm) => vm.NewCompanyLoading).WithConversion("BooleanInverted").Apply();


			this.txtKey.Text = "1";
			this.txtKey.Text = "";



		

			ViewHandler.Fire(ViewHandler.Event.ViewDidLoad);
		}

	
	}
}

