﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using MobileDocs.Core.Models;
using MobileDocs.Core.ViewModels;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;

namespace MobileDocs.Ios
{
	public partial class SideBar : UIViewController
	{
		private ProfileModule _profileModule;

		public SideBar (ProfileModule p) : base ("SideBar", null)
		{
		
			_profileModule = p;


	
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			base.NavigationController.NavigationBarHidden = false;
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			BrowseBarView BrowseBarFragment = new BrowseBarView(_profileModule);
			this.Add (BrowseBarFragment);
		}
	}
}

