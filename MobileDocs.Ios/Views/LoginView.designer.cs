// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace MobileDocs.Ios
{
	[Register ("LoginView")]
	partial class LoginView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnCancelLogin { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnChangeCompany { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnCompDialogAdd { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnCompDialogAddNew { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnCompDialogCancel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnLogin { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnSelectCompany { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnShowAddCompDialog { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imgLogin { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView indLoginLoading { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblLoading { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblLoginStatus { get; set; }

		[Outlet]
		MonoTouch.UIKit.UINavigationBar navSelectCompany { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIPickerView picChoiceCompany { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView spinLoadng { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIToolbar tbrBottom { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtCompDialogKey { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtLoginCmp { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtLoginPass { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtLoginUser { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView vieLoginWrapper { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView viewAddCompDialog { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView viewPickerWrapper { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView viwLoginStatus { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnCancelLogin != null) {
				btnCancelLogin.Dispose ();
				btnCancelLogin = null;
			}

			if (btnChangeCompany != null) {
				btnChangeCompany.Dispose ();
				btnChangeCompany = null;
			}

			if (btnCompDialogAdd != null) {
				btnCompDialogAdd.Dispose ();
				btnCompDialogAdd = null;
			}

			if (btnCompDialogAddNew != null) {
				btnCompDialogAddNew.Dispose ();
				btnCompDialogAddNew = null;
			}

			if (btnCompDialogCancel != null) {
				btnCompDialogCancel.Dispose ();
				btnCompDialogCancel = null;
			}

			if (btnLogin != null) {
				btnLogin.Dispose ();
				btnLogin = null;
			}

			if (btnSelectCompany != null) {
				btnSelectCompany.Dispose ();
				btnSelectCompany = null;
			}

			if (btnShowAddCompDialog != null) {
				btnShowAddCompDialog.Dispose ();
				btnShowAddCompDialog = null;
			}

			if (imgLogin != null) {
				imgLogin.Dispose ();
				imgLogin = null;
			}

			if (indLoginLoading != null) {
				indLoginLoading.Dispose ();
				indLoginLoading = null;
			}

			if (lblLoading != null) {
				lblLoading.Dispose ();
				lblLoading = null;
			}

			if (lblLoginStatus != null) {
				lblLoginStatus.Dispose ();
				lblLoginStatus = null;
			}

			if (picChoiceCompany != null) {
				picChoiceCompany.Dispose ();
				picChoiceCompany = null;
			}

			if (spinLoadng != null) {
				spinLoadng.Dispose ();
				spinLoadng = null;
			}

			if (tbrBottom != null) {
				tbrBottom.Dispose ();
				tbrBottom = null;
			}

			if (txtCompDialogKey != null) {
				txtCompDialogKey.Dispose ();
				txtCompDialogKey = null;
			}

			if (txtLoginCmp != null) {
				txtLoginCmp.Dispose ();
				txtLoginCmp = null;
			}

			if (txtLoginPass != null) {
				txtLoginPass.Dispose ();
				txtLoginPass = null;
			}

			if (txtLoginUser != null) {
				txtLoginUser.Dispose ();
				txtLoginUser = null;
			}

			if (vieLoginWrapper != null) {
				vieLoginWrapper.Dispose ();
				vieLoginWrapper = null;
			}

			if (viewAddCompDialog != null) {
				viewAddCompDialog.Dispose ();
				viewAddCompDialog = null;
			}

			if (viewPickerWrapper != null) {
				viewPickerWrapper.Dispose ();
				viewPickerWrapper = null;
			}

			if (viwLoginStatus != null) {
				viwLoginStatus.Dispose ();
				viwLoginStatus = null;
			}

			if (navSelectCompany != null) {
				navSelectCompany.Dispose ();
				navSelectCompany = null;
			}
		}
	}
}
