﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using MobileDocs.Core.Models;

namespace MobileDocs.Ios
{
	public partial class ModulesButtonCell : MvxCollectionViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("ModulesButtonCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ModulesButtonCell");

		public ModulesButtonCell (IntPtr handle) : base (handle)
		{

			this.DelayBind (() => {

				var set = this.CreateBindingSet<ModulesButtonCell,ProfileModule>();

//				set.Bind(imgLogo).To(p => p.Icon);
//				set.Bind(lblText).To(p => p.Name);
				set.Apply();

			});


		}

		public static ModulesButtonCell Create ()
		{
			return (ModulesButtonCell)Nib.Instantiate (null, null) [0];
		}
	}
}

