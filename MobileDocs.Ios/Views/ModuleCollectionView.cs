﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Collections.Generic;
using MobileDocs.Core.ViewModels;
using MonoTouch.ObjCRuntime;

namespace MobileDocs.Ios
{


	[Register("ModuleCollectionView")]
	public class ModuleCollectionView : MvxCollectionViewController
	{
		private bool _isInitialised;

		public ModuleCollectionView ()
			: base(new UICollectionViewFlowLayout()
				{
					ItemSize = new SizeF(240, 400),
					ScrollDirection = UICollectionViewScrollDirection.Horizontal
				})
		{		
			_isInitialised = true;
			ViewDidLoad();
		}

		public override void ViewDidLoad()
		{
			if (!_isInitialised)
				return;

			_isInitialised = true;

			var parent = ParentViewController;
			//base.ViewDidLoad();


			CollectionView.RegisterNibForCell(ModuleCollectionViewCell.Nib, ModuleCollectionViewCell.Key);
			var source = new MvxCollectionViewSource(CollectionView, ModuleCollectionViewCell.Key);
			CollectionView.Source = source;

			var set = this.CreateBindingSet<ModuleCollectionView, StartViewModel>();
			set.Bind(source).To(vm => vm.Modules);
			set.Apply();
			 

		}
	}
}

//
//	[Register("ModuleCollectionView")]
//	public class ModuleCollectionView : MvxCollectionViewController
//	{
//		private bool _isInitialised;
//
//		public ModuleCollectionView()
//			: base(new UICollectionViewFlowLayout()
//				{
//					ItemSize = new SizeF(240, 400),
//					ScrollDirection = UICollectionViewScrollDirection.Horizontal
//				})
//		{		
//
//	
//		}
//
//		public  override void ViewDidLoad()
//		{
//			var coll
//
//
////			CollectionView.RegisterNibForCell(ModuleCollectionViewCell.Nib, ModuleCollectionViewCell.Key);
////			var source = new MvxCollectionViewSource(CollectionView, ModuleCollectionViewCell.Key);
////			CollectionView.Source = source;
////
////			var set = this.CreateBindingSet<ModuleCollectionView, StartViewModel>();
////			set.Bind(source).To(vm => vm.Modules);
////			set.Apply();
////
////
////
////			CollectionView.ReloadData();
//		}
//	}
//}