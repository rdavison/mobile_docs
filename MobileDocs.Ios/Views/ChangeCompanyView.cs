﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MobileDocs.Ios.Common;

namespace MobileDocs.Ios
{
	public partial class ChangeCompanyView : UIViewController
	{

		public ViewHandler ViewHandler = new ViewHandler();

		private string SelectedProfile;


		public ChangeCompanyView () : base ("ChangeCompanyView", null)
		{
		}

	
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			ViewHandler.Fire(ViewHandler.Event.ViewDidLoad);
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			string test = "ad";

			ViewHandler.Fire(ViewHandler.Event.ViewWillDisappear, test);
		}
	}
}

