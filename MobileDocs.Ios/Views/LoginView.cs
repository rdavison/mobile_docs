﻿
using MobileDocs.Core.ViewModels;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Collections.Generic;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MobileDocs.Ios.Extension;
using System;
using Cirrious.MvvmCross.ViewModels;
using System.Drawing;
using MobileDocs.Ios.Common;
using Cirrious.MvvmCross.Binding.Touch.Views;

namespace MobileDocs.Ios
{
	public partial class LoginView : MvxViewController, IOSDialogHost<LoginView, LoginViewModel>
	{
		public static int instanceCount = 0;
		public int instanceNo = 0;

		private LoginViewModel _viewModel;
		private List<MvxPropertyChangedListener> _MvxPropertyChangedListeners = new List<MvxPropertyChangedListener> ();
		private UIImage _imgLoginHoriz;
		private UIImage _imgLoginVertic;
		private MvxFluentBindingDescriptionSet<LoginView, LoginViewModel> bindingSet;

		public LoginView() : base ("LoginView", null)
		{
			instanceCount++;
			instanceNo = instanceCount;
		}

		public LoginViewModel GetViewModel(){
			return _viewModel;
		}

		public MvxFluentBindingDescriptionSet<LoginView, LoginViewModel> GetBindingDescriptionSet(){
			return bindingSet;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

		//
		// Init
		//
			base.NavigationController.NavigationBarHidden = true;

			// Create Databing set
			_viewModel = ((LoginViewModel)this.DataContext);
			bindingSet = this.CreateBindingSet<LoginView, LoginViewModel> ();
			
			
			
		//
		// Add Navbar items
		//
//			this.Title = "Mobile Docs";
			ChoiceCompany choiceCmpCtrl = new ChoiceCompany();
			choiceCmpCtrl.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

			ChangeCompanyView ChangeCompanyCtrl = new ChangeCompanyView();
			ChangeCompanyCtrl.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;


//			UIBarButtonItem addCmpButton;
//			this.NavigationItem.SetRightBarButtonItem (
//				(addCmpButton = new UIBarButtonItem ("Add Company", UIBarButtonItemStyle.Done, null))
//			, true);
//
//			this.NavigationItem.SetLeftBarButtonItem (
//				new UIBarButtonItem ("Help", UIBarButtonItemStyle.Done, null)
//			, true);

		
		//
		// Change Company Spinner
		//
			var pickerViewModel = new MvxPickerViewModel(this.picChoiceCompany);
			//this.picChoiceCompany.DataSource;
			this.picChoiceCompany.Model = pickerViewModel;
			this.picChoiceCompany.ShowSelectionIndicator = true;

			bindingSet.Bind (pickerViewModel).For (p => p.SelectedItem).To (vm => vm.SelectedCompany).Apply();
			bindingSet.Bind(pickerViewModel).For(p => p.ItemsSource).To(vm => vm.Companies).Apply();

			//var _pt = viewPickerWrapper.Center;


	

			this.navSelectCompany.Items[0].RightBarButtonItems[0].Clicked += (object sender, EventArgs e) => { ToggleSelectComapnySpinner(); };
			this.btnSelectCompany.TouchUpInside += (object sender, EventArgs e) => { ToggleSelectComapnySpinner();};

		//
		// Choice Company Dialog.
		//


			//
			// Show or close the add Company Dialog - week binding therfor we add the listner to the listener it's belong. 
			//
			_MvxPropertyChangedListeners.Add(new MvxPropertyChangedListener(_viewModel).Listen(() => _viewModel.ShowAddCompany, (s, p) =>
			{
				// MvxPropertyChangedListener can be called even if the property is set to the same value.
				if(_viewModel.ShowAddCompany && !choiceCmpCtrl.IsBeingPresented) {	
					
					choiceCmpCtrl.Parent = this;
					this.PresentViewController(choiceCmpCtrl, true, null);
				}
				else if(!_viewModel.ShowAddCompany && !choiceCmpCtrl.IsBeingDismissed) {
					choiceCmpCtrl.DismissViewController(true, null);
				}

			}));


		//
		// Login Dialog
		//

			//Login
			bindingSet.Bind(this.btnLogin).To ((LoginViewModel vm) => vm.LoginCommand).Apply();

			this.btnLogin.SetTitleColor(UIColor.Clear.FromHex(0xd3d3d3), UIControlState.Disabled);

			bindingSet.Bind(this.txtLoginCmp).To((LoginViewModel vm) => vm.SelectedCompany).Apply();
			bindingSet.Bind(this.txtLoginPass).To((LoginViewModel vm) => vm.Username).Apply();
			bindingSet.Bind(this.txtLoginPass).To((LoginViewModel vm) => vm.Password).Apply();

		
			this.txtLoginCmp.UserInteractionEnabled = false;
		//
		//	SelectedCompany
		//

			UIBarButtonItem btnHelp = this.tbrBottom.Items[0];
			UIBarButtonItem btnAddCompany = this.tbrBottom.Items[2];
			bindingSet.Bind(btnHelp).To((LoginViewModel vm) => vm.ShowAddCompanyCommand).Apply();
			bindingSet.Bind(btnAddCompany).To((LoginViewModel vm) => vm.ShowAddCompanyCommand).Apply();
	
		
		//
		// Login progress bar
		//

			bindingSet.Bind(this.lblLoginStatus).To((LoginViewModel vm) => vm.LoadingText).Apply();
			bindingSet.Bind(this.btnCancelLogin).To((LoginViewModel vm) => vm.CancelLoginCommand).Apply();
			bindingSet.Bind(this.viwLoginStatus).For(spin => spin.Hidden).To((LoginViewModel vm) => vm.IsLoading).WithConversion("BooleanInverted").Apply();

		//
		// Rotation
		//
			_imgLoginHoriz = UIImage.FromBundle("login_screen_horizontal@2x.jpg");
			_imgLoginVertic = UIImage.FromBundle("login_screen_vertical@2x.jpg");
			HandleRotation();
		}



		public override void WillRotate (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate (toInterfaceOrientation, duration);

			HandleRotation();

		}

		/// <summary>
		/// Handles the rotation. 
		/// * Change background image of login screen depending on login screen
		/// </summary>
		private void HandleRotation() {
		
			if (MobileDocs.Ios.Common.Util.isLandScape ())  {
				imgLogin.Image = _imgLoginHoriz;
			} else if(MobileDocs.Ios.Common.Util.isPortrait ()) {
				imgLogin.Image = _imgLoginVertic;
			}

		}

		public override void ViewWillAppear (bool animated)
		{



			base.ViewWillAppear (animated);
			base.NavigationController.NavigationBarHidden = true;
		}
			

		private void ToggleSelectComapnySpinner ()
		{
			if(viewPickerWrapper.Hidden) {
				viewPickerWrapper.Hidden = false;
			}
			else {
				viewPickerWrapper.Hidden = true;
			}
		}

	}


}