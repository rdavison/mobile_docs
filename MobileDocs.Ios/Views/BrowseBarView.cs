﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using MobileDocs.Core.ViewModels;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Platform;
using MobileDocs.Core.Models;

namespace MobileDocs.Ios
{
	[Register("BrowseBarView")]
	public partial class BrowseBarView : MvxView
	{

		public BrowseBarView(IntPtr h): base(h)
		{

		}



		public BrowseBarView (ProfileModule _profileModule)
		{


			var arr = NSBundle.MainBundle.LoadNib("BrowseBarView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = new RectangleF(0, 0, 500, 500);
			AddSubview(v);


			//this.lblTest.Text = "stet";

			this.DelayBind(() => {


				//this.DataContext = DataContext;

				var set = this.CreateBindingSet<BrowseBarView, BrowseViewModel>();

		
				var rewrr = this.DataContext;


				//lblTest.Text = ((BrowseViewModel)rewrr).LoadingText;
				//set.Bind(this).For(t => t.DataContext).To
				set.Bind(lblTest).For(lbl => lbl.Text).To(b => b.LoadingText).Apply();
			});



			var browseParameters = new BrowseParameters () {
				RepoUrl = _profileModule.UrlBrowser,
				Type = BrowseType.Remote,
				RepoName = _profileModule.RepoName
			};

			var loaderService = Mvx.Resolve<IMvxViewModelLoader>();

			var viewModelRequest = MvxViewModelRequest<BrowseViewModel>.GetDefaultRequest ();
			viewModelRequest.ParameterValues = ((object)browseParameters).ToSimplePropertyDictionary ();
		
			this.DataContext = loaderService.LoadViewModel(viewModelRequest,  null);

			this.CreateBindingContext();



		}
	}
}