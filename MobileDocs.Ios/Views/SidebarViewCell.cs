﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace MobileDocs.Ios
{
	public class SidebarViewCell : UICollectionViewCell
	{
		public static readonly NSString Key = new NSString ("SidebarViewCell");

		[Export ("initWithFrame:")]
		public SidebarViewCell (RectangleF frame) : base (frame)
		{
			// TODO: add subviews to the ContentView, set various colors, etc.
			BackgroundColor = UIColor.Cyan;
		}
	}
}

