// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace MobileDocs.Ios
{
	[Register ("StartView")]
	partial class StartView
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView imgBackground { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imgBottomRightLogo { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblCompanyLogo { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView viewAccentColor { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView viewModulesBrn { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgBackground != null) {
				imgBackground.Dispose ();
				imgBackground = null;
			}

			if (imgBottomRightLogo != null) {
				imgBottomRightLogo.Dispose ();
				imgBottomRightLogo = null;
			}

			if (lblCompanyLogo != null) {
				lblCompanyLogo.Dispose ();
				lblCompanyLogo = null;
			}

			if (viewAccentColor != null) {
				viewAccentColor.Dispose ();
				viewAccentColor = null;
			}

			if (viewModulesBrn != null) {
				viewModulesBrn.Dispose ();
				viewModulesBrn = null;
			}
		}
	}
}
