using Cirrious.CrossCore.Plugins;

namespace MobileDocs.Store.Bootstrap
{
    public class WebBrowserPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.WebBrowser.PluginLoader>
    {
    }
}