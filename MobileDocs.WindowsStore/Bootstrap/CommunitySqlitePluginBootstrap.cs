using Cirrious.CrossCore.Plugins;

namespace MobileDocs.Store.Bootstrap
{
    public class SqlitePluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Community.Plugins.Sqlite.PluginLoader>
    {
    }
}