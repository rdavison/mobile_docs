﻿using System.Runtime.InteropServices;
using Cirrious.CrossCore;
using MobileDocs.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Storage.Streams;
using Windows.Security.Cryptography.Core;
using System.Runtime.InteropServices.WindowsRuntime;

namespace MobileDocs.Store.Common
{
    public class StoreEncryptionService : IEncryptionService
    {

        private const string ConstSalt = "WAkPwyaT,X#)n{DZAq&|Vc0&ynOj (f(t0C1#Nh-G5)X?%l.<i*Zf%j)K/uR.crI";
        private const string Password =  "G?14?EPak}~pg)C~wd=-4$a(gFp1<fI>Laxve]MESW~!z|b|8Rfl;Yj7`y/k{fWl";
        private const uint IterationCount = 10000;

        private static void PadToMultipleOf(ref byte[] src, int pad)
        {
            int len = (src.Length + pad - 1) / pad * pad;
            Array.Resize(ref src, len);
        }

        private static void GenerateKeyMaterial(string password, string salt, uint iterationCount, out IBuffer keyMaterial, out IBuffer iv)
        {
            // Setup KDF parameters for the desired salt and iteration count
            IBuffer saltBuffer = CryptographicBuffer.ConvertStringToBinary(salt, BinaryStringEncoding.Utf8);
            KeyDerivationParameters kdfParameters = KeyDerivationParameters.BuildForPbkdf2(saltBuffer, iterationCount);

            // Get a KDF provider for PBKDF2, and store the source password in a Cryptographic Key
            KeyDerivationAlgorithmProvider kdf = KeyDerivationAlgorithmProvider.OpenAlgorithm(KeyDerivationAlgorithmNames.Pbkdf2Sha1);
            IBuffer passwordBuffer = CryptographicBuffer.ConvertStringToBinary(password, BinaryStringEncoding.Utf8);
            CryptographicKey passwordSourceKey = kdf.CreateKey(passwordBuffer);

            // Generate key material from the source password, salt, and iteration count.  Only call DeriveKeyMaterial once,
            // since calling it twice will generate the same data for the key and IV.
            int keySize = 256 / 8;
            int ivSize = 128 / 8;
            uint totalDataNeeded = (uint)(keySize + ivSize);
            IBuffer keyAndIv = CryptographicEngine.DeriveKeyMaterial(passwordSourceKey, kdfParameters, totalDataNeeded);

            // Split the derived bytes into a seperate key and IV
            byte[] keyMaterialBytes = keyAndIv.ToArray();
            keyMaterial = WindowsRuntimeBuffer.Create(keyMaterialBytes, 0, keySize, keySize);
            iv = WindowsRuntimeBuffer.Create(keyMaterialBytes, keySize, ivSize, ivSize);
        }

        public byte[] Encrypt(byte[] toEncrypt, string salt)
        {
            IBuffer aesKeyMaterial;
            IBuffer iv;

            GenerateKeyMaterial(Password, ConstSalt + salt, IterationCount, out aesKeyMaterial, out iv);

            SymmetricKeyAlgorithmProvider aesProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
            CryptographicKey aesKey = aesProvider.CreateSymmetricKey(aesKeyMaterial);
            Mvx.Trace("length before padding = " + toEncrypt.Length);
            PadToMultipleOf(ref toEncrypt, (int)aesProvider.BlockLength);

            Mvx.Trace("length after padding = " + toEncrypt.Length);

            IBuffer plainText = CryptographicBuffer.CreateFromByteArray(toEncrypt);
            IBuffer encrypted = CryptographicEngine.Encrypt(aesKey, plainText, iv);

            byte[] byteArray;
            CryptographicBuffer.CopyToByteArray(encrypted, out byteArray);
            return byteArray;
        }

        public byte[] Decrypt(byte[] cipher, string salt)
        {
            
            IBuffer aesKeyMaterial;
            IBuffer iv;
            GenerateKeyMaterial(Password, ConstSalt + salt, IterationCount, out aesKeyMaterial, out iv);

            // Setup an AES key, using AES in CBC mode and applying PKCS#7 padding on the input
            SymmetricKeyAlgorithmProvider aesProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
            CryptographicKey aesKey = aesProvider.CreateSymmetricKey(aesKeyMaterial);
            Mvx.Trace("length before padding = " + cipher.Length);
            PadToMultipleOf(ref cipher, (int)aesProvider.BlockLength);

            Mvx.Trace("length after padding = " + cipher.Length);

            // Convert the base64 input to an IBuffer for decryption
            IBuffer ciphertext = CryptographicBuffer.CreateFromByteArray(cipher);

            // Decrypt the data and convert it back to a string
            IBuffer decrypted = CryptographicEngine.Decrypt(aesKey, ciphertext, iv);
            byte[] decryptedArray = decrypted.ToArray();
            return decryptedArray;
        }

        

        public string Encrypt(string toEncrypt, string salt)
        {
            byte[] encryptedBytes = Encrypt(Encoding.UTF8.GetBytes(toEncrypt), salt);
            return Convert.ToBase64String(encryptedBytes);
            //return Encoding.UTF8.GetString(encryptedBytes, 0, encryptedBytes.Length);
        }

        public string Decrypt(string cipher, string salt)
        {
            byte[] decryptedBytes = Decrypt(Convert.FromBase64String(cipher), salt);
            //remove empty bytes caused by encryption padding
            var i = decryptedBytes.Length - 1;
            while (decryptedBytes[i] == 0)
            {
                --i;
            }         
            return Encoding.UTF8.GetString(decryptedBytes, 0,i+1);
        }

        public string Sha256(string toHash)
        {
            IBuffer input = CryptographicBuffer.ConvertStringToBinary(toHash,
            BinaryStringEncoding.Utf8);
            var hasher = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256);
            return CryptographicBuffer.EncodeToHexString(hasher.HashData(input));
        }

        
    }
}
