using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace MobileDocs.Store.Common
{
    public class BooleanToOpacityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value is bool && (bool) value) ? 1 : 0.6;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}