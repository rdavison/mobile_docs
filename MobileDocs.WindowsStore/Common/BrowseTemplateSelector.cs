﻿using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MobileDocs.Store.Common
{
    class BrowseTemplateSelector : DataTemplateSelector
    {
        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            FileItem fileItem = item as FileItem;

            GridView grid = container as GridView;
            UIElement uiElement = container as UIElement;

            if (fileItem.IsDirectory)
            {
                //VariableSizedWrapGrid.SetColumnSpan(uiElement, 1);
                //VariableSizedWrapGrid.SetRowSpan(uiElement, 2);
                return App.Current.Resources["BrowseFolderItemTemplate"] as DataTemplate;
            }

            //VariableSizedWrapGrid.SetColumnSpan(uiElement, 1);
            //VariableSizedWrapGrid.SetRowSpan(uiElement, 1);

            return App.Current.Resources["BrowseFileItemTemplate"] as DataTemplate;
        }
    }
}
