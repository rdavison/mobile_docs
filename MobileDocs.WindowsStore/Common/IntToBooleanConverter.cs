using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace MobileDocs.Store.Common
{
    public class IntToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
            {
                return null;
            }
            return value is int && (int) value > -1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}