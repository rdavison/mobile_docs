﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//    Defines the BooleanToVisibilityConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace MobileDocs.Store.Common
{
    using System;
    using System.Collections;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Data;

    /// <summary>
    /// Value converter that translates true to <see cref="Visibility.Visible"/> and false to
    /// <see cref="Visibility.Collapsed"/>.
    /// </summary>
    public sealed class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null && value is ICollection)
            {
                return ((ICollection)value).Count > 0 ? Visibility.Visible : Visibility.Collapsed;
            }
            return value != null? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is Visibility && (Visibility)value == Visibility.Visible;
        }
    }
}