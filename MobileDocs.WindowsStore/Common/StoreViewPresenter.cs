﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.WindowsStore.Views;
using MobileDocs.Core.Common;

namespace MobileDocs.Store.Common
{
    class StoreViewPresenter : MvxStoreViewPresenter
    {
        private Frame rootFrame;
        public StoreViewPresenter(Frame rootFrame) : base(rootFrame)
        {
            this.rootFrame = rootFrame;
        }


        public override void ChangePresentation(MvxPresentationHint hint)
        {
            if (hint is ClearNavBackStackHint)
            {
                while (rootFrame.BackStack.Any())
                {
                    rootFrame.BackStack.Remove(rootFrame.BackStack.LastOrDefault());
                }
            }

            base.ChangePresentation(hint);
        }
    }
}
