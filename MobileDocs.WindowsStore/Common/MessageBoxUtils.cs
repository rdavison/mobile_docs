﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace MobileDocs.Store.Common
{
     [Flags]
    public enum MessageBoxButton
    {
      OK,
      Cancel,
      OKCancel = OK | Cancel,
    }
     
    public enum MessageBoxResult
   {
     OK,
     Cancel,
   }
    
   public static class MessageBox
   {
     public async static Task<MessageBoxResult> Show(string msg, string title, MessageBoxButton messageBoxButton)
     {
       var result = MessageBoxResult.Cancel;
       var md = new MessageDialog(msg, title);
       if (messageBoxButton.HasFlag(MessageBoxButton.OK))
       {
         md.Commands.Add(new UICommand("OK", new UICommandInvokedHandler(_ => result = MessageBoxResult.OK)));
       }
       if (messageBoxButton.HasFlag(MessageBoxButton.Cancel))
       {
         md.Commands.Add(new UICommand("Cancel", new UICommandInvokedHandler(_ => result = MessageBoxResult.Cancel)));
       }
       await md.ShowAsync();
       return result;
     }
   }
}
