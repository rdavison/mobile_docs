﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Cirrious.CrossCore;
using MobileDocs.Core.Models;

namespace MobileDocs.Store.Common
{
    public class SelectionChangedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (parameter is ListViewBase)
            {
                ListViewBase lvb = parameter as ListViewBase;
                List<object> objects = new List<object>(lvb.SelectedItems);
                return objects;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            string language)
        {
            throw new NotImplementedException();
        }
    }
}
