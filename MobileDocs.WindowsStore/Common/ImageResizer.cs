﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using MobileDocs.Core.Common;

namespace MobileDocs.Store.Common
{
    public class ImageResizer : IImageResizer
    {

        public async Task<byte[]> Resize(Stream stream, uint width, uint height, string extention, string encoding = null)
        {
            BitmapDecoder decoder = null;

            InMemoryRandomAccessStream ras = new InMemoryRandomAccessStream();
            await stream.CopyToAsync(ras.AsStreamForWrite());


           

            Guid encorderId = BitmapEncoder.JpegEncoderId;
            switch (extention)
            {
                case ".jpg":
                case ".jpeg":
                    decoder = await BitmapDecoder.CreateAsync(BitmapDecoder.JpegDecoderId, ras);
                    encorderId = BitmapEncoder.JpegEncoderId;
                    break;
                case ".png":
                    decoder = await BitmapDecoder.CreateAsync(BitmapDecoder.PngDecoderId, ras);
                    encorderId = BitmapEncoder.PngEncoderId;
                    break;
                case ".gif":
                    decoder = await BitmapDecoder.CreateAsync(BitmapDecoder.GifDecoderId, ras);
                    encorderId = BitmapEncoder.GifEncoderId;
                    break;
            }

            PixelDataProvider data = await decoder.GetPixelDataAsync();
            byte[] bytes = data.DetachPixelData();
            BitmapPropertySet propertySet = new BitmapPropertySet();
            // create class representing target jpeg quality - a bit obscure, but it works
            BitmapTypedValue qualityValue = new BitmapTypedValue(0.8, PropertyType.Single);
            propertySet.Add("ImageQuality", qualityValue);

          
            if (encoding != null)
            {
                 switch (encoding)
                {
                case ".jpg":
                case ".jpeg":
                    encorderId = BitmapEncoder.JpegEncoderId;
                    break;
                case ".png":
                    encorderId = BitmapEncoder.PngEncoderId;
                    break;
                case ".gif":
                    encorderId = BitmapEncoder.PngEncoderId;
                    break;
                }
            }

            uint imageWidth = decoder.PixelWidth;
            uint imageHeight = decoder.PixelHeight;

            BitmapEncoder be = await BitmapEncoder.CreateAsync(encorderId, ras, propertySet);
            be.SetPixelData(BitmapPixelFormat.Rgba8, BitmapAlphaMode.Straight, imageWidth, imageHeight, 96.0, 96.0, bytes);



            if (imageWidth > width || imageHeight > height)
            {
                BitmapBounds bounds = new BitmapBounds();
                if (imageWidth > width)
                {
                    bounds.Width = width;
                    bounds.X = (imageWidth - width) / 2;
                }
                else bounds.Width = imageWidth;
                if (imageHeight > height)
                {
                    bounds.Height = height;
                    bounds.Y = (imageHeight - height) / 2;
                }
                else bounds.Height = imageHeight;
                be.BitmapTransform.Bounds = bounds;
            }

            await be.FlushAsync();
            return new byte[0];
        }
    }
}
