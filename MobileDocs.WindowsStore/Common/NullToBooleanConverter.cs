﻿using Cirrious.CrossCore.WindowsCommon.Converters;
using MobileDocs.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileDocs.Store.Common
{
    public class NullToBooleanConverter : MvxNativeValueConverter<ConverterNullToBoolean>
    {
        //defined in core project IsNullConverter
    }
}
