﻿using MobileDocs.Core.Common;
using MobileDocs.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MobileDocs.Store.Common
{
    class NewsTemplateSelector : DataTemplateSelector
    {
        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            FeedItem feedItem = item as FeedItem;

            GridView grid = container as GridView;
            UIElement uiElement = container as UIElement;

            DataTemplate template = Application.Current.Resources["NewsItemTemplate"] as DataTemplate;

            if (feedItem.ImageUrl!=null)
            {
                VariableSizedWrapGrid.SetColumnSpan(uiElement, 1);
                VariableSizedWrapGrid.SetRowSpan(uiElement, 7);
                return  template;
            }

            VariableSizedWrapGrid.SetColumnSpan(uiElement, 1);
            VariableSizedWrapGrid.SetRowSpan(uiElement, 4);

            return template;
        }
    }
}
