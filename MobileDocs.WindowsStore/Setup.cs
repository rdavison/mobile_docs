using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.WindowsStore.Platform;
using Cirrious.MvvmCross.WindowsStore.Views;
using MobileDocs.Core.Common;
using MobileDocs.Core.Interfaces;
using MobileDocs.Store.Common;
using Windows.UI.Xaml.Controls;

namespace MobileDocs.Store
{
    public class Setup : MvxStoreSetup
    {
        public Setup(Frame rootFrame) : base(rootFrame)
        {
            rootFrame.Tapped += (sender, args) =>
            {
                //TODO send message to reset timer 
            };
        }

        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }
		
        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();
            Mvx.RegisterSingleton<IEncryptionService>(new StoreEncryptionService());
            Mvx.RegisterSingleton<IConnectionService>(new StoreConnectionService());
            Mvx.RegisterType<IImageResizer, ImageResizer>();
        }

        protected override IMvxStoreViewPresenter CreateViewPresenter(Frame rootFrame)
        {
            var presenter = new StoreViewPresenter(rootFrame);
            Mvx.RegisterSingleton(presenter);
            return presenter;
        }

        //protected override IMvxPhoneViewPresenter CreateViewPresenter(PhoneApplicationFrame rootFrame)
        //{
        //    var presenter = new CustomWP8ViewPresenter(rootFrame);
        //    Mvx.RegisterSingleton(presenter);
        //    return presenter;
        //}
    }
}