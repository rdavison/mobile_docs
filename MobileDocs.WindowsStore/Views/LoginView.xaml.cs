﻿using System;
using System.Diagnostics;
using Windows.System;
using Cirrious.MvvmCross.WindowsStore.Views;
using MobileDocs.Core.ViewModels;


namespace MobileDocs.Store.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginView : MvxStorePage
    {
        public LoginView()
        {
            this.InitializeComponent();
        }

        private void Password_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter && e.KeyStatus.RepeatCount == 1)
            {

                try
                {
                LoginViewModel dc = (LoginViewModel)this.DataContext;
                
                    if (dc.LoginCommand.CanExecute(null))
                    {
                        dc.LoginCommand.Execute(null);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);                
                }
            }
        }
    }
}
