﻿using System.Diagnostics;
using Windows.UI.Input;
using Cirrious.MvvmCross.WindowsStore.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MobileDocs.Store.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class StartView : MvxStorePage
    {
        private double _offsetPoint;

        public StartView()
        {
            
            this.InitializeComponent();
            _offsetPoint = GetXScrollOffset(Hub);
        }

        public double GetXScrollOffset (UIElement rootVisual)
        {

            if (rootVisual is Hub)
            {
                Hub hub = rootVisual as Hub;
                Point relativePoint = hub.Sections.First().TransformToVisual(this.Frame).TransformPoint(new Point(0, 0));
                return relativePoint.X;
            }

            return 0;
        }

        private void Hub_OnManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            //double offset = GetXScrollOffset(Hub);
            //GridBackgroundCt.TranslateX = ((offset-_offsetPoint)/5);
        }
    }
}
