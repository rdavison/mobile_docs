﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.WindowsStore.Views;
using MobileDocs.Core.ViewModels;
using pdftron;
using pdftron.PDF;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading.Tasks;

namespace MobileDocs.Store.Views
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DocumentView : MvxStorePage
    {
        private PDFViewCtrl _pdfViewCtrl;
        private IMvxMessenger _messenger;
        private MvxSubscriptionToken _token;

        private readonly Timer _timer;
        private bool _menuHidden = false;

        private void TimerEnded(object state)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
            {
                _menuHidden = true;
                VisualStateManager.GoToState(this, "HideMenu", true);
            });
        }

        public DocumentView()
        {

            _messenger = Mvx.Resolve<IMvxMessenger>();
            
            _timer = new Timer(TimerEnded,null,6000,-1);

            PDFNet.Initialize();
            this.InitializeComponent();

            ClickGrid.Tapped += Ctrl_Tapped;
            //PdfViewBorder.Tapped += Ctrl_Tapped;
           // 

            _pdfViewCtrl = new PDFViewCtrl();
            PdfViewBorder.Child = _pdfViewCtrl;
            _token = _messenger.SubscribeOnMainThread<DocumentMessage>(StreamReceived);
        }

        void Ctrl_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (_menuHidden)
            {
                _menuHidden = false;
                VisualStateManager.GoToState(this, "ShowMenu", true);
                _timer.Change(6000, -1);
            }
            else
            {
                _menuHidden = true;
                VisualStateManager.GoToState(this, "HideMenu", true);
                _timer.Change(0, 0);
            }
        }

        private async void StreamReceived(DocumentMessage obj)
        {
            try
            {
                switch (obj.FileExtention.Substring(1))
                {
                    case "pdf":
                        PDFDoc doc = new PDFDoc(obj.Bytes);
                        _pdfViewCtrl.SetDoc(doc);
                        ImageHolder.Visibility = Visibility.Collapsed;
                        break;
                    case "jpg":
                    case "jpeg":
                        ImageHolder.Source = await ToImage(obj.Bytes);
                        PdfViewBorder.Visibility = Visibility.Collapsed;
                        break;
                }
                
            }
            catch(Exception ex)
            {
                
            }
        }

        public async Task<BitmapImage> ToImage(byte[] array)
        {
            using (InMemoryRandomAccessStream raStream =
        new InMemoryRandomAccessStream())
            {
                using (DataWriter writer = new DataWriter(raStream))
                {
                    // Write the bytes to the stream
                    writer.WriteBytes(array);

                    // Store the bytes to the MemoryStream
                    await writer.StoreAsync();

                    // Not necessary, but do it anyway
                    await writer.FlushAsync();

                    // Detach from the Memory stream so we don't close it
                    writer.DetachStream();
                }

                raStream.Seek(0);

                BitmapImage bitMapImage = new BitmapImage();
                bitMapImage.SetSource(raStream);

                return bitMapImage;
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            _messenger.Unsubscribe<DocumentMessage>(_token);
            base.OnNavigatingFrom(e);

        }

        public void ZoomIn_OnClick(object sender, RoutedEventArgs e)
        {
            _pdfViewCtrl.SetZoom(_pdfViewCtrl.GetZoom()*1.25);
            ImageScrollViewer.ZoomToFactor(ImageScrollViewer.ZoomFactor * 1.25f);
        }

        public void ZoomOut_OnClick(object sender, RoutedEventArgs e)
        {
            ImageScrollViewer.ZoomToFactor(ImageScrollViewer.ZoomFactor * 0.75f);
            _pdfViewCtrl.SetZoom(_pdfViewCtrl.GetZoom() * 0.75);
        }

        private void ImageScrollViewer_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            //ImageHolder.Width = ImageScrollViewer.ViewportWidth;
            //ImageHolder.Height = ImageScrollViewer.ViewportWidth;
        }

    }
}
